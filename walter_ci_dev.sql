-- phpMyAdmin SQL Dump
-- version 3.5.0
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 15, 2014 at 03:24 PM
-- Server version: 5.1.62-log
-- PHP Version: 5.3.15

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `walter_ci_dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `business_units`
--

CREATE TABLE IF NOT EXISTS `business_units` (
  `business_unit_id` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `business_unit` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`business_unit_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `business_units`
--

INSERT INTO `business_units` (`business_unit_id`, `business_unit`) VALUES
(1, 'Contract'),
(2, 'SBD'),
(3, 'Retail'),
(4, 'Internal Comm'),
(5, 'SBG'),
(6, 'International'),
(7, 'Agency'),
(8, 'Brand'),
(9, 'Quill'),
(10, 'eCommerce'),
(11, 'MAP');

-- --------------------------------------------------------

--
-- Stand-in structure for view `business_units_view`
--
CREATE TABLE IF NOT EXISTS `business_units_view` (
`business_unit_id` int(12) unsigned
,`business_unit` varchar(200)
,`category_count` bigint(21)
,`vehicle_count` bigint(21)
);
-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `business_unit_id` int(11) DEFAULT NULL,
  `active` int(1) DEFAULT '1',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=184 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category`, `business_unit_id`, `active`) VALUES
(1, 'Customer Development', NULL, 1),
(2, 'Rewards', NULL, 1),
(3, 'Field Marketing', NULL, 1),
(4, 'Credit and Business Accounts', NULL, 1),
(5, 'Store Programs', NULL, 1),
(6, 'Contract Technology Solutions', NULL, 1),
(7, 'Copy & Print', NULL, 1),
(8, 'Easy Tech', NULL, 1),
(9, 'Office Supplies', NULL, 1),
(10, 'Promo Products', NULL, 1),
(11, 'Technology', NULL, 1),
(12, 'Staples Industrial', NULL, 1),
(13, 'Furniture', NULL, 1),
(14, 'Promotions, Seasons Events', NULL, 1),
(15, 'Site Development', NULL, 1),
(16, 'AB Testing', NULL, 1),
(17, 'Mass Media (TV, Circular, Radio)', NULL, 1),
(18, 'Interactive & New Media', NULL, 1),
(19, 'Sports Marketing', NULL, 1),
(20, 'Public Relations', NULL, 1),
(21, 'Investor Relations', NULL, 1),
(22, 'Human Resources', NULL, 1),
(23, 'Special Events', NULL, 1),
(24, 'Last Resort', NULL, 1),
(25, 'SBG Other', NULL, 1),
(26, 'Contract Promotional Products', NULL, 1),
(27, 'Retail Other', NULL, 1),
(28, 'SBD Merchandising', NULL, 1),
(29, 'SBD Other', NULL, 1),
(30, 'Site Maintenance - Support Track', NULL, 1),
(31, 'Internal Communications Other', NULL, 1),
(32, 'Staples Soul', NULL, 1),
(33, 'International Other', NULL, 1),
(34, 'Acquisition', NULL, 1),
(35, 'Contract Business Interiors', NULL, 1),
(36, 'Contract Facilities Solutions', NULL, 1),
(37, 'Contract Print Solutions', NULL, 1),
(38, 'Contract Office Products', NULL, 1),
(39, 'Administration', NULL, 1),
(40, 'Branding', NULL, 1),
(41, 'Strategic Markets', NULL, 1),
(42, 'Online Circular', NULL, 1),
(43, 'Facilities', NULL, 1),
(44, 'Sustainable Earth', NULL, 1),
(45, 'Contract Field Marketing', NULL, 1),
(46, 'Contract Programs', NULL, 1),
(47, 'Contract Other', NULL, 1),
(48, 'Gift Cards', NULL, 1),
(49, 'Education', NULL, 1),
(50, 'Medical', NULL, 1),
(68, 'Administration', 7, 1),
(69, 'Site Development', 7, 1),
(70, 'Site Maintenance - Support Track', 7, 1),
(71, 'Special Events', 7, 1),
(72, 'Technology', 7, 1),
(73, 'Administration', 8, 1),
(74, 'Branding', 8, 1),
(75, 'Administration', 1, 1),
(76, 'Contract Business Interiors', 1, 1),
(77, 'Contract Facilities Solutions', 1, 1),
(78, 'Contract Field Marketing', 1, 1),
(79, 'Contract Office Products', 1, 1),
(80, 'Contract Other', 1, 1),
(81, 'Contract Print Solutions', 1, 1),
(82, 'Contract Programs', 1, 1),
(83, 'Contract Promotional Products', 1, 1),
(84, 'Contract Technology Solutions', 1, 1),
(85, 'AB Testing', 10, 1),
(86, 'Acquisition', 10, 0),
(87, 'Administration', 10, 1),
(88, 'Site Development', 10, 1),
(89, 'Site Maintenance - Support Track', 10, 1),
(90, 'Administration', 4, 1),
(91, 'Branding', 4, 1),
(92, 'Human Resources', 4, 1),
(93, 'Interactive & New Media', 4, 1),
(94, 'Internal Communications Other', 4, 1),
(95, 'International Other', 4, 1),
(96, 'Investor Relations', 4, 1),
(97, 'Public Relations', 4, 1),
(98, 'Special Events', 4, 1),
(99, 'Staples Soul', 4, 1),
(100, 'Administration', 6, 1),
(101, 'Site Development', 6, 1),
(102, 'Acquisition', 11, 1),
(103, 'Administration', 11, 1),
(104, 'Branding', 11, 1),
(105, 'Customer Development', 11, 1),
(106, 'Education', 11, 1),
(107, 'Facilities', 11, 1),
(108, 'Field Marketing', 11, 1),
(109, 'Furniture', 11, 1),
(110, 'Human Resources', 11, 1),
(111, 'Medical', 11, 1),
(112, 'Office Supplies', 11, 1),
(113, 'Promo Products', 11, 1),
(114, 'Promotions, Seasons Events', 11, 1),
(115, 'Rewards', 11, 1),
(116, 'Site Maintenance - Support Track', 11, 1),
(117, 'Special Events', 11, 1),
(118, 'Technology', 11, 1),
(119, 'Acquisition', 9, 1),
(120, 'Administration', 9, 1),
(121, 'Branding', 9, 1),
(122, 'Customer Development', 9, 1),
(123, 'Education', 9, 1),
(124, 'Facilities', 9, 1),
(125, 'Field Marketing', 9, 1),
(126, 'Furniture', 9, 1),
(127, 'Human Resources', 9, 1),
(128, 'Medical', 9, 1),
(129, 'Office Supplies', 9, 1),
(130, 'Promo Products', 9, 1),
(131, 'Promotions, Seasons Events', 9, 1),
(132, 'Rewards', 9, 1),
(133, 'Site Maintenance - Support Track', 9, 1),
(134, 'Special Events', 9, 1),
(135, 'Technology', 9, 1),
(136, 'Acquisition', 3, 1),
(137, 'Administration', 3, 1),
(138, 'Copy & Print', 3, 1),
(139, 'Credit and Business Accounts', 3, 1),
(140, 'Customer Development', 3, 1),
(141, 'Field Marketing', 3, 1),
(142, 'Furniture', 3, 1),
(143, 'Gift Cards', 3, 1),
(144, 'Interactive & New Media', 3, 1),
(145, 'Internal Communications Other', 3, 1),
(146, 'Mass Media (TV, Circular, Radio)', 3, 1),
(147, 'Office Supplies', 3, 1),
(148, 'Promotions, Seasons Events', 3, 1),
(149, 'Public Relations', 3, 1),
(150, 'Retail Other', 3, 1),
(151, 'Rewards', 3, 1),
(152, 'Special Events', 3, 1),
(153, 'Sports Marketing', 3, 1),
(154, 'Strategic Markets', 3, 1),
(155, 'Technology', 3, 1),
(156, 'Acquisition', 2, 1),
(157, 'Administration', 2, 1),
(158, 'Copy & Print', 2, 1),
(159, 'Customer Development', 2, 1),
(160, 'Facilities', 2, 1),
(161, 'Furniture', 2, 1),
(162, 'Interactive & New Media', 2, 1),
(163, 'Office Supplies', 2, 1),
(164, 'Online Circular', 2, 1),
(165, 'Promotions, Seasons Events', 2, 1),
(166, 'Rewards', 2, 1),
(167, 'SBD Merchandising', 2, 1),
(168, 'SBD Other', 2, 1),
(169, 'Site Development', 2, 1),
(170, 'Site Maintenance - Support Track', 2, 1),
(171, 'Special Events', 2, 1),
(172, 'Staples Industrial', 2, 1),
(173, 'Technology', 2, 1),
(174, 'Administration', 5, 1),
(175, 'Facilities', 5, 1),
(176, 'Furniture', 5, 1),
(177, 'Interactive & New Media', 5, 1),
(178, 'Internal Communications Other', 5, 1),
(179, 'Office Supplies', 5, 1),
(180, 'Promotions, Seasons Events', 5, 1),
(181, 'SBG Other', 5, 1),
(182, 'Technology', 5, 1),
(183, 'Test', 7, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE IF NOT EXISTS `jobs` (
  `job_id` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `effort_code` varchar(32) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `job_name` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `status_id` int(12) DEFAULT NULL,
  `user_id` int(12) DEFAULT NULL,
  `job_type_id` int(12) DEFAULT NULL,
  `marketing_region_id` int(12) DEFAULT NULL,
  `business_unit_id` int(12) DEFAULT NULL,
  `category_id` int(12) DEFAULT NULL,
  `tier_id` int(2) DEFAULT NULL,
  `mp_display` int(1) DEFAULT NULL,
  `mp_description` text,
  `mp_search` text,
  `vehicle_id` int(12) DEFAULT NULL,
  `hit_date` date DEFAULT NULL,
  `notes` text,
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`job_id`, `effort_code`, `created`, `modified`, `job_name`, `status_id`, `user_id`, `job_type_id`, `marketing_region_id`, `business_unit_id`, `category_id`, `tier_id`, `mp_display`, `mp_description`, `mp_search`, `vehicle_id`, `hit_date`, `notes`) VALUES
(2, '123', '0000-00-00 00:00:00', '2014-08-14 18:49:09', 'Test job 2', 1, 1308, 2, 1, 9, 0, 1, 0, 'description test2', 'search test 2', 1, '2014-07-18', 'test notes 2'),
(3, '', '0000-00-00 00:00:00', '2014-08-01 16:12:17', 'test job 3', 1, 1308, 1, 10, 3, 3, 3, 1, 'description test3', 'search test3', 1, '2014-07-13', 'test notes 3'),
(4, '', '0000-00-00 00:00:00', '2014-08-01 16:12:17', 'test job 4', 1, 1308, 2, 1, 1, 1, 1, 0, 'description test 4', 'search test 4', 1, '2014-08-13', 'test notes 4'),
(5, '', '0000-00-00 00:00:00', '2014-08-11 17:46:29', 'test job 5', 1, 1308, 2, 1, 3, 144, 1, 0, 'description test 5', 'search test 5', 239, '2014-08-31', 'test notes 5'),
(7, '', '0000-00-00 00:00:00', '2014-08-01 16:12:17', 'Test Today', 1, 1308, 1, 1, 1, 17, 1, 0, '', '', 3, '2014-07-18', ''),
(8, '', '0000-00-00 00:00:00', '2014-08-01 16:12:17', 'Geoff test', 1, 1308, 1, 10, 1, 3, 1, 1, 'fake description', 'contract, online', 4, '2014-07-12', 'fake notes'),
(9, '', '0000-00-00 00:00:00', '2014-08-01 16:12:17', 'Geoff Test Geoff test Geoff ', 1, 1308, 2, 10, 7, 69, 1, 1, 'geoff test 3', '', 83, '2014-07-26', 'test'),
(10, '1', '0000-00-00 00:00:00', '2014-08-01 16:12:17', 'Test 6-30', 1, 1308, 1, 2, 9, 123, 1, 1, '', '', 208, '2014-07-31', '');

-- --------------------------------------------------------

--
-- Stand-in structure for view `jobs_view`
--
CREATE TABLE IF NOT EXISTS `jobs_view` (
`job_id` int(12) unsigned
,`effort_code` varchar(32)
,`owner` varchar(401)
,`job_name` varchar(200)
,`job_type` varchar(200)
,`status` varchar(200)
,`business_unit` varchar(200)
,`category` varchar(200)
,`tier` varchar(200)
,`vehicle` varchar(200)
,`marketing_region` varchar(200)
,`mp_description` text
,`mp_search` text
,`mp_display` int(1)
,`notes` text
,`hit_date` date
,`rate` varchar(20)
);
-- --------------------------------------------------------

--
-- Table structure for table `job_types`
--

CREATE TABLE IF NOT EXISTS `job_types` (
  `job_type_id` int(12) NOT NULL AUTO_INCREMENT,
  `job_type` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`job_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `job_types`
--

INSERT INTO `job_types` (`job_type_id`, `job_type`) VALUES
(1, 'Online'),
(2, 'Offline');

-- --------------------------------------------------------

--
-- Table structure for table `marketing_regions`
--

CREATE TABLE IF NOT EXISTS `marketing_regions` (
  `marketing_region_id` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `marketing_region` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`marketing_region_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `marketing_regions`
--

INSERT INTO `marketing_regions` (`marketing_region_id`, `marketing_region`) VALUES
(1, 'Australia'),
(2, 'Canada'),
(3, 'China'),
(4, 'Europe'),
(5, 'Germany'),
(6, 'India'),
(7, 'New Zealand'),
(8, 'South America'),
(9, 'UK'),
(10, 'US');

-- --------------------------------------------------------

--
-- Table structure for table `rate_cards`
--

CREATE TABLE IF NOT EXISTS `rate_cards` (
  `rate_card_id` int(11) NOT NULL AUTO_INCREMENT,
  `marketing_region_id` int(11) DEFAULT NULL,
  `business_unit_id` int(11) DEFAULT NULL,
  `vehicle_id` int(11) DEFAULT NULL,
  `tier_id` int(11) DEFAULT NULL,
  `rate` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`rate_card_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=59 ;

--
-- Dumping data for table `rate_cards`
--

INSERT INTO `rate_cards` (`rate_card_id`, `marketing_region_id`, `business_unit_id`, `vehicle_id`, `tier_id`, `rate`) VALUES
(1, 10, 1, 109, 1, '75-200'),
(2, 10, 1, 106, 1, '30'),
(3, 10, 3, 239, 1, '100-150'),
(4, 10, 3, 240, 1, '45'),
(6, 10, 3, 241, 1, '350'),
(7, 10, 3, 242, 1, '60'),
(8, 10, 3, 245, 1, '450'),
(9, 10, 3, 244, 1, '85'),
(10, 10, 3, 246, 1, '140'),
(11, 10, 3, 251, 1, '60'),
(12, 10, 3, 253, 1, '95'),
(13, 10, 3, 255, 1, '125'),
(14, 10, 3, 258, 1, '500'),
(15, 10, 3, 263, 1, '200'),
(16, 10, 3, 265, 1, '45'),
(17, 10, 3, 266, 1, '200'),
(18, 10, 3, 268, 1, '250-300'),
(19, 10, 3, 271, 1, '150'),
(20, 10, 3, 272, 1, '150'),
(21, 10, 3, 238, 2, '30'),
(22, 10, 3, 346, 2, '12'),
(23, 10, 3, 239, 2, '40'),
(24, 10, 3, 240, 2, '35'),
(25, 10, 3, 3, 2, '75'),
(26, 10, 3, 48, 2, '110'),
(27, 10, 3, 242, 2, '30'),
(28, 10, 3, 244, 2, '40'),
(29, 10, 3, 246, 2, '35'),
(30, 10, 3, 247, 2, '40'),
(31, 10, 3, 249, 2, '40'),
(32, 10, 3, 251, 2, '30'),
(33, 10, 3, 253, 2, '50'),
(34, 10, 3, 254, 2, '20'),
(35, 10, 3, 255, 2, '30'),
(36, 10, 3, 257, 2, '40'),
(37, 10, 3, 258, 2, '100'),
(38, 10, 3, 263, 2, '50'),
(39, 10, 3, 265, 2, '10'),
(40, 10, 3, 266, 2, '25'),
(41, 10, 3, 268, 2, '75-150'),
(42, 10, 3, 272, 2, '50'),
(43, 10, 3, 52, 3, '2'),
(44, 10, 3, 239, 3, '10'),
(45, 10, 3, 240, 3, '10'),
(46, 10, 3, 242, 3, '10'),
(47, 10, 3, 244, 3, '5'),
(48, 10, 3, 246, 3, '15'),
(49, 10, 3, 251, 3, '10'),
(50, 10, 3, 254, 3, '10'),
(51, 10, 3, 255, 3, '15'),
(52, 10, 3, 258, 3, '25'),
(53, 10, 3, 263, 3, '5'),
(54, 10, 3, 265, 3, '3'),
(55, 10, 3, 266, 3, '10'),
(56, 10, 3, 272, 3, '5'),
(57, 1, 8, 88, 3, '5'),
(58, 1, 1, 106, 3, '11');

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE IF NOT EXISTS `statuses` (
  `status_id` int(12) NOT NULL AUTO_INCREMENT,
  `status` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `statuses`
--

INSERT INTO `statuses` (`status_id`, `status`) VALUES
(1, 'Active'),
(2, 'Completed'),
(3, 'Deleted');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `task_id` int(8) NOT NULL AUTO_INCREMENT,
  `task_type_id` int(8) NOT NULL DEFAULT '0',
  `job_id` int(8) NOT NULL,
  `timesheet_id` int(8) NOT NULL,
  `sun` decimal(4,2) unsigned zerofill NOT NULL,
  `mon` decimal(4,2) unsigned zerofill NOT NULL,
  `tue` decimal(4,2) unsigned zerofill NOT NULL,
  `wed` decimal(4,2) unsigned zerofill NOT NULL,
  `thu` decimal(4,2) unsigned zerofill NOT NULL,
  `fri` decimal(4,2) unsigned zerofill NOT NULL,
  `sat` decimal(4,2) unsigned zerofill NOT NULL,
  `total` decimal(4,2) unsigned zerofill NOT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`task_id`, `task_type_id`, `job_id`, `timesheet_id`, `sun`, `mon`, `tue`, `wed`, `thu`, `fri`, `sat`, `total`) VALUES
(2, 0, 2, 1, '01.00', '01.00', '01.00', '01.00', '02.00', '02.00', '02.00', '10.00'),
(3, 0, 3, 1, '02.00', '02.00', '02.00', '02.00', '02.00', '02.00', '02.00', '14.00'),
(4, 0, 4, 1, '02.00', '02.00', '02.00', '04.00', '02.00', '02.00', '02.00', '16.00'),
(5, 3, 5, 6, '02.00', '02.00', '02.00', '02.00', '02.00', '02.00', '02.00', '14.00'),
(7, 0, 7, 6, '02.00', '02.00', '02.00', '02.00', '02.00', '02.00', '02.00', '14.00'),
(12, 0, 2, 6, '00.00', '00.00', '00.00', '00.00', '00.00', '00.00', '00.00', '00.00'),
(15, 3, 0, 1, '01.00', '01.00', '02.00', '01.00', '05.00', '00.00', '00.00', '10.00'),
(16, 5, 0, 1, '00.00', '05.00', '00.00', '05.00', '00.00', '05.00', '00.00', '15.00'),
(17, 0, 2, 10, '00.00', '00.00', '00.00', '00.00', '00.00', '00.00', '00.00', '00.00');

-- --------------------------------------------------------

--
-- Stand-in structure for view `task_totals`
--
CREATE TABLE IF NOT EXISTS `task_totals` (
`timesheet_id` int(12)
,`Sunday` decimal(26,2)
,`Monday` decimal(26,2)
,`Tuesday` decimal(26,2)
,`Wednesday` decimal(26,2)
,`Thursday` decimal(26,2)
,`Friday` decimal(26,2)
,`Saturday` decimal(26,2)
,`Total` decimal(26,2)
);
-- --------------------------------------------------------

--
-- Table structure for table `task_types`
--

CREATE TABLE IF NOT EXISTS `task_types` (
  `task_type_id` int(2) NOT NULL AUTO_INCREMENT,
  `task_type_name` varchar(125) NOT NULL,
  PRIMARY KEY (`task_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `task_types`
--

INSERT INTO `task_types` (`task_type_id`, `task_type_name`) VALUES
(1, 'Vacation Time'),
(2, 'Sick Time'),
(3, 'Holiday Time'),
(4, 'Non-Billable'),
(5, 'Personal Time'),
(6, 'Training'),
(7, 'Approved Out of Office');

-- --------------------------------------------------------

--
-- Table structure for table `tiers`
--

CREATE TABLE IF NOT EXISTS `tiers` (
  `tier_id` int(12) NOT NULL AUTO_INCREMENT,
  `tier` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`tier_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tiers`
--

INSERT INTO `tiers` (`tier_id`, `tier`) VALUES
(1, 'Tier 1'),
(2, 'Tier 2'),
(3, 'Tier 3');

-- --------------------------------------------------------

--
-- Table structure for table `timesheets`
--

CREATE TABLE IF NOT EXISTS `timesheets` (
  `timesheet_id` int(12) NOT NULL AUTO_INCREMENT,
  `user_id` int(12) NOT NULL,
  `week_ending` date NOT NULL,
  `status_id` int(12) NOT NULL,
  PRIMARY KEY (`timesheet_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `timesheets`
--

INSERT INTO `timesheets` (`timesheet_id`, `user_id`, `week_ending`, `status_id`) VALUES
(1, 1308, '2014-07-26', 1),
(6, 1308, '2014-07-12', 1),
(10, 1308, '2014-08-16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE IF NOT EXISTS `vehicles` (
  `vehicle_id` int(12) unsigned NOT NULL AUTO_INCREMENT,
  `vehicle` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `business_unit_id` int(11) DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`vehicle_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=347 ;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`vehicle_id`, `vehicle`, `business_unit_id`, `active`) VALUES
(1, 'Statement', NULL, 1),
(2, 'Insert - Statement or Box', NULL, 1),
(3, 'Catalog Covers', NULL, 1),
(4, 'Email', NULL, 1),
(5, 'Email Module', NULL, 1),
(6, 'Landing Page', NULL, 1),
(7, 'Microsite', NULL, 1),
(8, 'Website', NULL, 1),
(9, 'Shippers-Promo Fixture', NULL, 1),
(10, 'Packaging System', NULL, 1),
(11, 'Product Packaging', NULL, 1),
(12, 'Naming-Logo Lockup', NULL, 1),
(13, 'Sell Sheet', NULL, 1),
(14, 'User Guide', NULL, 1),
(15, 'Signage', NULL, 1),
(16, 'Print Ad or ROP', NULL, 1),
(17, 'Circular', NULL, 1),
(18, 'Tabloid', NULL, 1),
(19, 'Flyer', NULL, 1),
(20, 'Home Office Event', NULL, 1),
(21, 'Fax', NULL, 1),
(22, 'TV', NULL, 1),
(23, 'Press Kit', NULL, 1),
(24, 'Newsletter', NULL, 1),
(25, 'Video', NULL, 1),
(26, 'Media - Other', NULL, 1),
(27, 'Photography', NULL, 1),
(28, 'Gate Crashers', NULL, 1),
(29, 'Guidelines', NULL, 1),
(30, 'Other', NULL, 1),
(31, 'Poster', NULL, 1),
(32, 'Powerpoint Presentation Template', NULL, 1),
(33, 'Tradeshow Booth Graphics', NULL, 1),
(34, 'Video Production', NULL, 1),
(35, 'Video with Flash', NULL, 1),
(36, 'Web Banner', NULL, 1),
(37, 'Web Page (Store-Front, Category Front, Broadcast Bulletin, Landing Page)', NULL, 1),
(38, 'Coupon Book, Computer or Tech Guilde', NULL, 1),
(39, 'Meeting in a Box', NULL, 1),
(40, 'Outdoor Signage or Banners', NULL, 1),
(41, 'Planning', NULL, 1),
(42, 'Signage - Concept Development Only', NULL, 1),
(43, 'Cross Channel Concept Development', NULL, 1),
(44, 'Insert - Credit Card Statement or Box', NULL, 1),
(45, 'SBD Online Campaign  includes Email, LP and Banner', NULL, 1),
(46, 'Cards, gift cards, savings cards, rewards cards', NULL, 1),
(47, 'Direct Mail - all other', NULL, 1),
(48, 'Catalog Text Pages', NULL, 1),
(49, 'Catalog Index', NULL, 1),
(50, 'Catalog Page Template', NULL, 1),
(51, 'Catalog Ads', NULL, 1),
(52, 'Banner - Standard', NULL, 1),
(53, 'Banner - Rich Media', NULL, 1),
(54, 'Special Events', NULL, 1),
(55, 'New Media Update', NULL, 1),
(56, 'Brochure', NULL, 1),
(57, 'Administration', NULL, 1),
(58, 'Radio', NULL, 1),
(59, 'Application (facebook ap, DEB, Games, etc)', NULL, 1),
(60, 'Outdoor', NULL, 1),
(61, 'Broadband', NULL, 1),
(62, 'Development', NULL, 1),
(63, 'Concept Development', NULL, 1),
(64, 'Case Study', NULL, 1),
(65, 'Content Management', NULL, 1),
(66, 'Direct Mail - Postcard, Popcard, Selfmailer, Letter Package', NULL, 1),
(67, 'Event  (Landing Page, Email Module and Banner Resize)', NULL, 1),
(68, 'Packaging', NULL, 1),
(69, 'Social Media', NULL, 1),
(78, 'Administration', 7, 1),
(79, 'Application (facebook ap, DEB, Games, etc)', 7, 1),
(80, 'Concept Development', 7, 1),
(81, 'Content Management', 7, 1),
(82, 'Development', 7, 1),
(83, 'Planning', 7, 1),
(84, 'Poster', 7, 1),
(85, 'Signage', 7, 1),
(86, 'Social Media', 7, 1),
(87, 'Administration', 8, 1),
(88, 'Application (facebook ap, DEB, Games, etc)', 8, 1),
(89, 'Brochure', 8, 1),
(90, 'Concept Development', 8, 1),
(91, 'Flyer', 8, 1),
(92, 'Guidelines', 8, 1),
(93, 'Home Office Event', 8, 1),
(94, 'Poster', 8, 1),
(95, 'Radio', 8, 1),
(96, 'Signage', 8, 1),
(97, 'Signage - Concept Development Only', 8, 1),
(98, 'Social Media', 8, 1),
(99, 'Special Events', 8, 1),
(100, 'Video', 8, 1),
(101, 'Video Production', 8, 1),
(102, 'Video with Flash', 8, 1),
(103, 'Website', 8, 1),
(104, 'Administration', 1, 1),
(105, 'Brochure', 1, 1),
(106, 'Case Study', 1, 1),
(107, 'Catalog Ads', 1, 1),
(108, 'Catalog Covers', 1, 1),
(109, 'Catalog Index', 1, 1),
(110, 'Catalog Page Template', 1, 1),
(111, 'Catalog Text Pages', 1, 1),
(112, 'Concept Development', 1, 1),
(113, 'Content Management', 1, 1),
(114, 'Direct Mail - all other', 1, 1),
(115, 'Email', 1, 1),
(116, 'Flyer', 1, 1),
(117, 'Gate Crashers', 1, 1),
(118, 'Guidelines', 1, 1),
(119, 'Microsite', 1, 1),
(120, 'Newsletter', 1, 1),
(121, 'Other', 1, 1),
(122, 'Planning', 1, 1),
(123, 'Poster', 1, 1),
(124, 'Powerpoint Presentation Template', 1, 1),
(125, 'Print Ad or ROP', 1, 1),
(126, 'Sell Sheet', 1, 1),
(127, 'Social Media', 1, 1),
(128, 'Tradeshow Booth Graphics', 1, 1),
(129, 'User Guide', 1, 1),
(130, 'Video Production', 1, 1),
(131, 'Video with Flash', 1, 1),
(132, 'Web Banner', 1, 1),
(133, 'Web Page (Store-Front, Category Front, Broadcast Bulletin, Landing Page)', 1, 1),
(134, 'Website', 1, 1),
(135, 'Administration', 10, 1),
(136, 'Concept Development', 10, 1),
(137, 'Content Management', 10, 1),
(138, 'Development', 10, 1),
(139, 'Guidelines', 10, 1),
(140, 'Microsite', 10, 1),
(141, 'Social Media', 10, 1),
(142, 'Administration', 4, 1),
(143, 'Application (facebook ap, DEB, Games, etc)', 4, 1),
(144, 'Banner - Rich Media', 4, 1),
(145, 'Brochure', 4, 1),
(146, 'Email', 4, 1),
(147, 'Email Module', 4, 1),
(148, 'Flyer', 4, 1),
(149, 'Guidelines', 4, 1),
(150, 'Home Office Event', 4, 1),
(151, 'Landing Page', 4, 1),
(152, 'Media - Other', 4, 1),
(153, 'Meeting in a Box', 4, 1),
(154, 'Microsite', 4, 1),
(155, 'Naming-Logo Lockup', 4, 1),
(156, 'New Media Update', 4, 1),
(157, 'Newsletter', 4, 1),
(158, 'Other', 4, 1),
(159, 'Outdoor Signage or Banners', 4, 1),
(160, 'Photography', 4, 1),
(161, 'Planning', 4, 1),
(162, 'Poster', 4, 1),
(163, 'Powerpoint Presentation Template', 4, 1),
(164, 'Print Ad or ROP', 4, 1),
(165, 'Radio', 4, 1),
(166, 'Sell Sheet', 4, 1),
(167, 'Signage', 4, 1),
(168, 'Social Media', 4, 1),
(169, 'Tradeshow Booth Graphics', 4, 1),
(170, 'TV', 4, 1),
(171, 'User Guide', 4, 1),
(172, 'Video', 4, 1),
(173, 'Video Production', 4, 1),
(174, 'Video with Flash', 4, 1),
(175, 'Web Banner', 4, 1),
(176, 'Web Page (Store-Front, Category Front, Broadcast Bulletin, Landing Page)', 4, 1),
(177, 'Administration', 6, 1),
(178, 'Social Media', 6, 1),
(179, 'Website', 6, 1),
(180, 'Administration', 11, 1),
(181, 'Banner - Rich Media', 11, 1),
(182, 'Banner - Standard', 11, 1),
(183, 'Brochure', 11, 1),
(184, 'Catalog Ads', 11, 1),
(185, 'Catalog Covers', 11, 1),
(186, 'Catalog Page Template', 11, 1),
(187, 'Catalog Text Pages', 11, 1),
(188, 'Concept Development', 11, 1),
(189, 'Coupon Book, Computer or Tech Guilde', 11, 1),
(190, 'Cross Channel Concept Development', 11, 1),
(191, 'Direct Mail - all other', 11, 1),
(192, 'Email', 11, 1),
(193, 'Event  (Landing Page, Email Module and Banner Resize)', 11, 1),
(194, 'Flyer', 11, 1),
(195, 'Insert - Credit Card Statement or Box', 11, 1),
(196, 'Landing Page', 11, 1),
(197, 'Other', 11, 1),
(198, 'Packaging', 11, 1),
(199, 'Photography', 11, 1),
(200, 'Poster', 11, 1),
(201, 'Print Ad or ROP', 11, 1),
(202, 'Sell Sheet', 11, 1),
(203, 'Social Media', 11, 1),
(204, 'Tabloid', 11, 1),
(205, 'Video', 11, 1),
(206, 'Web Banner', 11, 1),
(207, 'Website', 11, 1),
(208, 'Administration', 9, 1),
(209, 'Banner - Rich Media', 9, 1),
(210, 'Banner - Standard', 9, 1),
(211, 'Brochure', 9, 1),
(212, 'Catalog Ads', 9, 1),
(213, 'Catalog Covers', 9, 1),
(214, 'Catalog Page Template', 9, 1),
(215, 'Catalog Text Pages', 9, 1),
(216, 'Concept Development', 9, 1),
(217, 'Coupon Book, Computer or Tech Guilde', 9, 1),
(218, 'Cross Channel Concept Development', 9, 1),
(219, 'Direct Mail - Postcard, Popcard, Selfmailer, Letter Package', 9, 1),
(220, 'Email', 9, 1),
(221, 'Event  (Landing Page, Email Module and Banner Resize)', 9, 1),
(222, 'Flyer', 9, 1),
(223, 'Insert - Statement or Box', 9, 1),
(224, 'Landing Page', 9, 1),
(225, 'Other', 9, 1),
(226, 'Packaging', 9, 1),
(227, 'Photography', 9, 1),
(228, 'Poster', 9, 1),
(229, 'Print Ad or ROP', 9, 1),
(230, 'Sell Sheet', 9, 1),
(231, 'Social Media', 9, 1),
(232, 'Tabloid', 9, 1),
(233, 'Video', 9, 1),
(234, 'Web Banner', 9, 1),
(235, 'Website', 9, 1),
(236, 'Administration', 3, 1),
(237, 'Application (facebook ap, DEB, Games, etc)', 3, 1),
(238, 'Banner - Rich Media', 3, 1),
(239, 'Brochure', 3, 1),
(240, 'Cards, gift cards, savings cards, rewards cards', 3, 1),
(241, 'Circular', 3, 1),
(242, 'Concept Development', 3, 1),
(243, 'Content Management', 3, 1),
(244, 'Coupon Book, Computer or Tech Guilde', 3, 1),
(245, 'Cross Channel Concept Development', 3, 1),
(246, 'Direct Mail - Postcard, Popcard, Selfmailer, Letter Package', 3, 1),
(247, 'Email', 3, 1),
(248, 'Email Module', 3, 1),
(249, 'Event  (Landing Page, Email Module and Banner Resize)', 3, 1),
(250, 'Fax', 3, 1),
(251, 'Flyer', 3, 1),
(252, 'Guidelines', 3, 1),
(253, 'Home Office Event', 3, 1),
(254, 'Insert - Credit Card Statement or Box', 3, 1),
(255, 'Landing Page', 3, 1),
(256, 'Media - Other', 3, 1),
(257, 'Meeting in a Box', 3, 1),
(258, 'Microsite', 3, 1),
(259, 'Naming-Logo Lockup', 3, 1),
(260, 'Other', 3, 1),
(261, 'Outdoor Signage or Banners', 3, 1),
(262, 'Packaging', 3, 1),
(263, 'Photography', 3, 1),
(264, 'Print Ad or ROP', 3, 1),
(265, 'Radio', 3, 1),
(266, 'Signage', 3, 1),
(267, 'Signage - Concept Development Only', 3, 1),
(268, 'Social Media', 3, 1),
(269, 'Statement', 3, 1),
(270, 'Tradeshow Booth Graphics', 3, 1),
(271, 'TV', 3, 1),
(272, 'Video', 3, 1),
(273, 'Web Banner', 3, 1),
(274, 'Administration', 2, 1),
(275, 'Application (facebook ap, DEB, Games, etc)', 2, 1),
(276, 'Banner - Rich Media', 2, 1),
(277, 'Broadband', 2, 1),
(278, 'Brochure', 2, 1),
(279, 'Cards, gift cards, savings cards, rewards cards', 2, 1),
(280, 'Catalog Ads', 2, 1),
(281, 'Catalog Covers', 2, 1),
(282, 'Catalog Index', 2, 1),
(283, 'Catalog Page Template', 2, 1),
(284, 'Catalog Text Pages', 2, 1),
(285, 'Concept Development', 2, 1),
(286, 'Content Management', 2, 1),
(287, 'Coupon Book, Computer or Tech Guilde', 2, 1),
(288, 'Cross Channel Concept Development', 2, 1),
(289, 'Direct Mail - Postcard, Popcard, Selfmailer, Letter Package', 2, 1),
(290, 'Email', 2, 1),
(291, 'Email Module', 2, 1),
(292, 'Fax', 2, 1),
(293, 'Guidelines', 2, 1),
(294, 'Insert - Credit Card Statement or Box', 2, 1),
(295, 'Landing Page', 2, 1),
(296, 'Microsite', 2, 1),
(297, 'Naming-Logo Lockup', 2, 1),
(298, 'New Media Update', 2, 1),
(299, 'Other', 2, 1),
(300, 'Photography', 2, 1),
(301, 'Print Ad or ROP', 2, 1),
(302, 'Radio', 2, 1),
(303, 'SBD Online Campaign  includes Email, LP and Banner', 2, 1),
(304, 'Signage', 2, 1),
(305, 'Social Media', 2, 1),
(306, 'Statement', 2, 1),
(307, 'Tabloid', 2, 1),
(308, 'Video Production', 2, 1),
(309, 'Video with Flash', 2, 1),
(310, 'Web Banner', 2, 1),
(311, 'Website', 2, 1),
(312, 'Administration', 5, 1),
(313, 'Application (facebook ap, DEB, Games, etc)', 5, 1),
(314, 'Banner - Rich Media', 5, 1),
(315, 'Brochure', 5, 1),
(316, 'Concept Development', 5, 1),
(317, 'Coupon Book, Computer or Tech Guilde', 5, 1),
(318, 'Direct Mail - all other', 5, 1),
(319, 'Email', 5, 1),
(320, 'Email Module', 5, 1),
(321, 'Event  (Landing Page, Email Module and Banner Resize)', 5, 1),
(322, 'Flyer', 5, 1),
(323, 'Guidelines', 5, 1),
(324, 'Home Office Event', 5, 1),
(325, 'Insert - Statement or Box', 5, 1),
(326, 'Landing Page', 5, 1),
(327, 'Meeting in a Box', 5, 1),
(328, 'Microsite', 5, 1),
(329, 'Naming-Logo Lockup', 5, 1),
(330, 'Newsletter', 5, 1),
(331, 'Packaging System', 5, 1),
(332, 'Photography', 5, 1),
(333, 'Poster', 5, 1),
(334, 'Press Kit', 5, 1),
(335, 'Print Ad or ROP', 5, 1),
(336, 'Product Packaging', 5, 1),
(337, 'Shippers-Promo Fixture', 5, 1),
(338, 'Signage', 5, 1),
(339, 'Signage - Concept Development Only', 5, 1),
(340, 'Social Media', 5, 1),
(341, 'Special Events', 5, 1),
(342, 'Tradeshow Booth Graphics', 5, 1),
(343, 'Video', 5, 1),
(344, 'Web Banner', 5, 1),
(345, 'add', 7, 0),
(346, 'Banner - Standard', 3, 1);

-- --------------------------------------------------------

--
-- Structure for view `business_units_view`
--
DROP TABLE IF EXISTS `business_units_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `business_units_view` AS select `business_units`.`business_unit_id` AS `business_unit_id`,`business_units`.`business_unit` AS `business_unit`,count(distinct `categories`.`category`) AS `category_count`,count(distinct `vehicles`.`vehicle`) AS `vehicle_count` from ((`business_units` left join `categories` on((`categories`.`business_unit_id` = `business_units`.`business_unit_id`))) left join `vehicles` on((`vehicles`.`business_unit_id` = `business_units`.`business_unit_id`))) group by `business_units`.`business_unit` order by `business_units`.`business_unit_id`;

-- --------------------------------------------------------

--
-- Structure for view `jobs_view`
--
DROP TABLE IF EXISTS `jobs_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `jobs_view` AS select `j`.`job_id` AS `job_id`,`j`.`effort_code` AS `effort_code`,concat(convert(`u`.`first_name` using utf8),' ',`u`.`last_name`) AS `owner`,`j`.`job_name` AS `job_name`,`jt`.`job_type` AS `job_type`,`s`.`status` AS `status`,`bu`.`business_unit` AS `business_unit`,`c`.`category` AS `category`,`t`.`tier` AS `tier`,`v`.`vehicle` AS `vehicle`,`mr`.`marketing_region` AS `marketing_region`,`j`.`mp_description` AS `mp_description`,`j`.`mp_search` AS `mp_search`,`j`.`mp_display` AS `mp_display`,`j`.`notes` AS `notes`,`j`.`hit_date` AS `hit_date`,`r`.`rate` AS `rate` from (((((((((`jobs` `j` left join `statuses` `s` on((`s`.`status_id` = `j`.`status_id`))) left join `job_types` `jt` on((`jt`.`job_type_id` = `j`.`job_type_id`))) left join `user_admin`.`users` `u` on((`u`.`user_id` = `j`.`user_id`))) left join `marketing_regions` `mr` on((`mr`.`marketing_region_id` = `j`.`marketing_region_id`))) left join `business_units` `bu` on((`bu`.`business_unit_id` = `j`.`business_unit_id`))) left join `categories` `c` on((`c`.`category_id` = `j`.`category_id`))) left join `tiers` `t` on((`t`.`tier_id` = `j`.`tier_id`))) left join `vehicles` `v` on((`v`.`vehicle_id` = `j`.`vehicle_id`))) left join `rate_cards` `r` on((`r`.`vehicle_id` = `j`.`vehicle_id`)));

-- --------------------------------------------------------

--
-- Structure for view `task_totals`
--
DROP TABLE IF EXISTS `task_totals`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `task_totals` AS select `timesheets`.`timesheet_id` AS `timesheet_id`,ifnull(sum(`tasks`.`sun`),0) AS `Sunday`,ifnull(sum(`tasks`.`mon`),0) AS `Monday`,ifnull(sum(`tasks`.`tue`),0) AS `Tuesday`,ifnull(sum(`tasks`.`wed`),0) AS `Wednesday`,ifnull(sum(`tasks`.`thu`),0) AS `Thursday`,ifnull(sum(`tasks`.`fri`),0) AS `Friday`,ifnull(sum(`tasks`.`sat`),0) AS `Saturday`,ifnull(sum(`tasks`.`total`),0) AS `Total` from (`timesheets` left join `tasks` on((`tasks`.`timesheet_id` = `timesheets`.`timesheet_id`))) group by `tasks`.`timesheet_id` order by `timesheets`.`timesheet_id`;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
