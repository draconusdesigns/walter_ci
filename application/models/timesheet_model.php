<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Timesheet_model extends CI_Model {

	function __construct() {
		
	}
	
	function createTimesheet($user_id){
		//get the next week ending date
		$saturday = date('Y-m-d',strtotime("next Saturday"));
			
		$query = array(
			'week_ending' => $saturday,
			'user_id' => $user_id,
			'status_id' => '1'
		);
		//query the db to check if there is a timesheet for that user for this week ending date	
		$this->db->where('week_ending', $query['week_ending']);
		$this->db->where('user_id', $query['user_id']);
		$this->db->limit(1);
		$test = $this->db->get("timesheets");
		
		//if there is a current timesheet for this week for this user, then return FALSE else insert and return TRUE
		if ( $test->num_rows() > 0 ) {
            return FALSE;
		}else{
			$this->db->insert('timesheets', $query);
	        return TRUE;
		}
	}
	
	function countTimesheets($user_id = null){
		if($user_id){
			$this->db->where('user_id', $user_id);
		}
		
		$this->db->from('timesheets');
		$count = $this->db->count_all_results();
		
        return $count;
    }
	
	function getPaginatedTimesheets($user_id = null, $limit, $offset = 0) {
		$this->db->join('statuses', 'statuses.status_id = timesheets.status_id');
		
		if($user_id){
			$this->db->where('user_id', $user_id);
		}
		
		$this->db->limit($limit, $offset);
		$this->db->order_by("week_ending", "desc");
		$timesheets = $this->db->get("timesheets");
		
		//Get Task Totals for Each Timesheet
		if ($timesheets->num_rows() > 0):
			foreach ($timesheets->result() as $timesheet):
				$this->db->select('sum(total) as total');
				$this->db->where('timesheet_id', $timesheet->timesheet_id);
				$timesheet->total = $this->db->get('tasks')->row()->total;
			endforeach;
		endif;
				
		return $timesheets->result();
	}
	
	function getTimesheets($user_id = null) {
		$this->db->join('statuses', 'statuses.status_id = timesheets.status_id');
		
		if($user_id){
			$this->db->where('user_id', $user_id);
		}
		
		$timesheets = $this->db->get("timesheets");
		
		//Get Task Totals for Each Timesheet
		if ($timesheets->num_rows() > 0):
			foreach ($timesheets->result() as $timesheet):
				$this->db->select('sum(total) as total');
				$this->db->where('timesheet_id', $timesheet->timesheet_id);
				$timesheet->total = $this->db->get('tasks')->row()->total;
			endforeach;
		endif;
		
		return $timesheets->result();
	}

	function getTimesheetsNonBillable($user_id = null) {
		$first_week_ending = date( 'Y-m-d' , strtotime('January First Saturday of' . date("Y")));		
		
		$this->db->select('sum(tasks.total) as total, tasks.task_type_id, task_types.task_type_name as task_name, round(sum(tasks.total)/8,2) as days', false);
		$this->db->join('tasks', 'tasks.timesheet_id = timesheets.timesheet_id');
		$this->db->join('task_types', 'task_types.task_type_id = tasks.task_type_id');
		$this->db->where('user_id', $user_id);
		$this->db->where('tasks.task_type_id !=', 0);
		$this->db->where('week_ending >=', $first_week_ending);
		$this->db->group_by("task_type_id"); 
		$query = $this->db->get("timesheets");
		
		return $query->result();
	}
	
	function getYearDropdown() {
		$this->db->distinct();
		$query = $this->db->get('week_ending');		
		
		$data = array(' ' => '--Select a Year--');
		
		foreach ($query->result_array() as $row){
			$data[$row['week_ending']] = $row['week_ending'];
		}		
		return $data;
	}
	
	
	function addTimesheet($data) {
		//get the next week ending date
		$saturday = date('Y-m-d',strtotime("next Saturday"));
			
		$query = array(
			'week_ending' => $this->input->post('week_ending'),
			'user_id' => $this->input->post('user_id'),
			'status_id' => '1'
		);
		//query the db to check if there is a timesheet for that user for this week ending date	
		$this->db->where('week_ending', $query['week_ending']);
		$this->db->where('user_id', $query['user_id']);
		$this->db->limit(1);
		$test = $this->db->get("timesheets");
		
		//if there is a current timesheet for this week for this user, then return FALSE else insert and return TRUE
		if ( $test->num_rows() > 0 ) {
            return FALSE;
		}else{
			$this->db->insert('timesheets', $query);
	        return TRUE;
		}
	}
	
	function getTimesheet($timesheet_id) {
		$query = $this->db->get_where('timesheets', array('timesheet_id' => $timesheet_id));		
		return $query->row();
	}
	
	function getCurrentTimesheet($user_id){
		$saturday = date('Y-m-d',strtotime("next Saturday"));
		
		$this->db->where('week_ending', $saturday);
		$this->db->where('user_id', $user_id);
		$this->db->limit(1);
		$query = $this->db->get("timesheets");
		
		return $query->row();
	}
	
	function addTasktoCurrentTimesheet($job_id, $timesheet_id){
		if(!empty($job_id) && !empty($timesheet_id) ){
			//query the db to check if there is currently a job with this job id, if not, then die
			$this->db->where('job_id', $job_id);
			$job_id_test = $this->db->get("jobs");
		
			if ($job_id_test->num_rows() == 0){
				$this->session->set_flashdata('error','No Job exists with that ID');
			} 
		
			//query the db to check if there is currently a task on this timesheet for this job
			$this->db->where('job_id', $job_id);
			$this->db->where('timesheet_id', $timesheet_id);
			$task_test = $this->db->get("tasks");

			//check if there is already a task for this job in this timesheet, if not then add it to the timesheet.
			if ($task_test->num_rows() == 0){
				$this->db->set('timesheet_id', $timesheet_id); 
				$this->db->set('job_id', $job_id); 
				$this->db->insert('tasks');
			
				$this->session->set_flashdata('success','Task Added to Timesheet Successfully');
				return true;
			}
			$this->session->set_flashdata('error','Task Already Exists on this Timesheet');
			return false;
		}
	}
	
	function addTask($data){
		$timesheet_id = $this->input->post('timesheet_id');
		$job_id = $this->input->post('job_id');
		$effort_code = $this->input->post('effort_code');
		$task_type_id = $this->input->post('task_type_id');
		
		if(!empty($effort_code)){
			//query the db to check if there is a job with this effort_code; if so, set the job_id, if not, then die 
			$this->db->where('effort_code', $effort_code);
			$this->db->limit(1);
			$effort_code_test = $this->db->get("jobs");
			
			if ($effort_code_test->num_rows() > 0){
				//set the job id for the result with that effort code
				foreach ($effort_code_test->result_array() as $row){
					$job_id = $row['job_id'];
				}
			}else{
				$this->session->set_flashdata('error','No Job exists with that Effort Code');
				return false;
			}
		}
		if(!empty($job_id)){
			//query the db to check if there is currently a job with this job id, if not, then die
			$this->db->where('job_id', $job_id);
			$job_id_test = $this->db->get("jobs");
		
			if ($job_id_test->num_rows() == 0){
				$this->session->set_flashdata('error','No Job exists with that ID');
				return false;
			} 
		
			//query the db to check if there is currently a task on this timesheet for this job
			$this->db->where('job_id', $job_id);
			$this->db->where('timesheet_id', $timesheet_id);
			$task_test = $this->db->get("tasks");

			//check if there is already a task for this job in this timesheet, if not then add it to the timesheet.
			if ($task_test->num_rows() == 0){
				$this->db->set('timesheet_id', $timesheet_id); 
				$this->db->set('job_id', $job_id); 
				$this->db->insert('tasks');
			
				$this->session->set_flashdata('success','Task Added to Timesheet Successfully');
				return true;
			}
			
			$this->session->set_flashdata('error','Task Already Exists on this Timesheet');
			return false;
		}
		if(!empty($task_type_id)){
			//query the db to check if there is currently a task on this timesheet with this task type
			$this->db->where('task_type_id', $task_type_id);
			$this->db->where('timesheet_id', $timesheet_id);
			$task_type_test = $this->db->get("tasks");

			//check if there is already a task for this task type in this timesheet, if not then add it to the timesheet.
			if ($task_type_test->num_rows() == 0){
				$this->db->set('timesheet_id', $timesheet_id); 
				$this->db->set('task_type_id', $task_type_id); 
				$this->db->insert('tasks');
			
				$this->session->set_flashdata('success','Task Added to Timesheet Successfully');
				return true;
			}
		}	
		$this->session->set_flashdata('error','This Task Cannot be added to this Timesheet');
		return false;
	}
 
	function getTasksForTimesheet($timesheet_id) {
		$this->db->where('timesheet_id', $timesheet_id);
		$this->db->join('jobs', 'jobs.job_id = tasks.job_id', 'left');
		$this->db->join('task_types', 'task_types.task_type_id = tasks.task_type_id', 'left');
		$query = $this->db->get("tasks");
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
		return FALSE;
	}
	function copyTasks($user_id, $timesheet_id){
		//Get this Timesheet's Week Ending
		$saturday = $this->db->select('week_ending')->get_where('timesheets', array('timesheet_id' => $timesheet_id))->row()->week_ending;
		
		//Get the week Before's Week Ending for current timesheet
		$last_saturday = date('Y-m-d',strtotime(' -7 days', strtotime($saturday)));
		
		//Get Last Weeks Tasks		
		$tasks = $this->db->select('task_type_id, job_id')->where('user_id', $user_id)->where('week_ending', $last_saturday)->join('timesheets', 'tasks.timesheet_id = timesheets.timesheet_id')->get("tasks");
		
		//Add them to this timesheet if there are any
		if ($tasks->num_rows() > 0):
			foreach ($tasks->result() as $task):
				$this->db->set('timesheet_id', $timesheet_id); 
				$this->db->set('task_type_id', $task->task_type_id);
				$this->db->set('job_id', $task->job_id);
			
				$this->db->insert('tasks');
			endforeach;
			$this->session->set_flashdata('success','Tasks Added to Timesheet Successfully');
			return true;
		else:
			$this->session->set_flashdata('error','No Tasks Found Last Week');
			return false;
		endif;
	}
	function getTask($task_id) {
		$this->db->join('jobs', 'jobs.job_id = tasks.job_id');		
		$query = $this->db->get_where('tasks', array('task_id' => $task_id));
		return $query->row();
	}
	
	function getTaskTypes() {
		$query = $this->db->get('task_types');	
		
		$data = array(' ' => '--Select Non-Job related time--');
							
		foreach ($query->result_array() as $row){
			$data[$row['task_type_id']] = $row['task_type_name'];
		}
				
		return $data;
	}
	
	function removeTask($user_id, $timesheet_id, $task_id) {
		//verify that the task belongs to the currently logged in user
		$this->db->join('timesheets', 'timesheets.timesheet_id = tasks.timesheet_id');
		$this->db->where('task_id', $task_id);
		$this->db->where('user_id', $user_id);
		$user_test = $this->db->get('tasks');
		
		if($user_test->num_rows() > 0):
			$this->db->where('task_id', $task_id);
			$this->db->delete('tasks');
			$this->session->set_flashdata('success','Task successfully removed from timesheet');
			return true;
		endif;
		
		$this->session->set_flashdata('error','You do not have permission to do this...');
		return false;
	}
	
	function updateTimesheet($task_id, $day, $value) {
		//update record in database (called from ajax in timesheet.js)
		$this->db->where('task_id', $task_id);

		$query = array(
			$day => $value
		);

		$this->db->update('tasks', $query);
		
		//update task total after the above update occurs
		
		$this->db->query('UPDATE tasks SET total = sun + mon + tue + wed + thu + fri + sat where task_id =' . $task_id);
	}
	
	function removeEmptyTasks($user_id, $timesheet_id) {
		//verify that the task belongs to the currently logged in user
		$this->db->where('timesheet_id', $timesheet_id);
		$this->db->where('user_id', $user_id);
		$user_test = $this->db->get('timesheets');
		
		if($user_test->num_rows() > 0):
			$this->db->where('sun', '00.00');
			$this->db->where('mon', '00.00');
			$this->db->where('tue', '00.00');
			$this->db->where('wed', '00.00');
			$this->db->where('thu', '00.00');
			$this->db->where('fri', '00.00');
			$this->db->where('sat', '00.00');
			$this->db->where('timesheet_id', $timesheet_id);
			
			$this->db->delete('tasks');
			return true;
		endif;
		
		return false;
	}
	
	function updateTimesheetStatus($timesheet_id, $status_id){
		$query = array(
			'status_id' => $status_id
		);
		$this->db->where('timesheet_id', $timesheet_id);
		$this->db->update('timesheets', $query);
		$this->session->set_flashdata('success','Timesheet status updated successfully');
	}
}