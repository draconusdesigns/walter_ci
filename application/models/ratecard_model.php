<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ratecard_model extends CI_Model {

	function __construct() {
		
	}
	function selectAllRatecards() {
		$this->db->join('marketing_regions', 'marketing_regions.marketing_region_id = rate_cards.marketing_region_id');
		$this->db->join('business_units', 'business_units.business_unit_id = rate_cards.business_unit_id');
		$this->db->join('vehicles', 'vehicles.vehicle_id = rate_cards.vehicle_id');
		$this->db->join('tiers', 'tiers.tier_id = rate_cards.tier_id');
		
		$query = $this->db->get("rate_cards");
		return $query->result();
	}

	function countRatecards(){
        return $this->db->count_all('rate_cards');
    }

	function getPaginatedRatecards($limit, $offset = 0) {
		$this->db->join('marketing_regions', 'marketing_regions.marketing_region_id = rate_cards.marketing_region_id');
		$this->db->join('business_units', 'business_units.business_unit_id = rate_cards.business_unit_id');
		$this->db->join('vehicles', 'vehicles.vehicle_id = rate_cards.vehicle_id');
		$this->db->join('tiers', 'tiers.tier_id = rate_cards.tier_id');
		
		$this->db->limit($limit, $offset);
		$query = $this->db->get("rate_cards");
		return $query->result();
	}
	
	function addRatecard($data) {
		$query = array(
			'marketing_region_id' => $this->input->post('marketing_region_id'),	
			'business_unit_id' => $this->input->post('business_unit_id'),	
			'vehicle_id' => $this->input->post('vehicle_id'),	
			'tier_id' => $this->input->post('tier_id'),	
			'rate' => $this->input->post('rate')	
		);
				
		$this->db->insert('rate_cards', $query);		
	}	
	
	function updateRatecard($data) {
		$rate_card_id = $this->input->post('rate_card_id');
		
		$query = array(
			'marketing_region_id' => $this->input->post('marketing_region_id'),	
			'business_unit_id' => $this->input->post('business_unit_id'),	
			'vehicle_id' => $this->input->post('vehicle_id'),	
			'tier_id' => $this->input->post('tier_id'),	
			'rate' => $this->input->post('rate')
		);
		
		$this->db->where('rate_card_id', $rate_card_id);
		$this->db->update('rate_cards', $query);					
	}
	
	function getRatecard($rate_card_id) {
		$this->db->join('marketing_regions', 'marketing_regions.marketing_region_id = rate_cards.marketing_region_id');
		$this->db->join('business_units', 'business_units.business_unit_id = rate_cards.business_unit_id');
		$this->db->join('vehicles', 'vehicles.vehicle_id = rate_cards.vehicle_id');
		$this->db->join('tiers', 'tiers.tier_id = rate_cards.tier_id');
				
		$query = $this->db->get_where('rate_cards', array('rate_card_id' => $rate_card_id));		
		return $query->row();
	}
	
	function getRatecardView($rate_card_id) {
		$this->db->join('marketing_regions', 'marketing_regions.marketing_region_id = rate_cards.marketing_region_id');
		$this->db->join('business_units', 'business_units.business_unit_id = rate_cards.business_unit_id');
		$this->db->join('vehicles', 'vehicles.vehicle_id = rate_cards.vehicle_id');
		$this->db->join('tiers', 'tiers.tier_id = rate_cards.tier_id');
		
		$query = $this->db->get_where('rate_cards', array('rate_card_id' => $rate_card_id));		
		return $query->row();
	}	
}