<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Job_model extends CI_Model {

	function __construct() {
		
	}
	
	function selectAllJobs() {
		return $this->db->join('statuses', 'statuses.status_id = jobs.status_id')->get("jobs")->result();
	}

	function countJobs($job_search_terms=null){
		if (!empty($job_search_terms)){
			//map header field names to db field names and to themselves for recursive queries
			$db_fields = array(
				'job' => 'job_id',
				'name' => 'job_name',
				'owner' => 'owner',
				'status' => 'status',
				'effort code' => 'effort_code',
				'business unit' => 'business_unit',
				'hit date' => 'hit_date',
				'region' => 'marketing_region',
				'job_id' => 'job_id',
				'job_name' => 'job_name',
				'effort_code' => 'effort_code',
				'business_unit' => 'business_unit',
				'hit_date' => 'hit_date',
				'marketing_region' => 'marketing_region'
			);
			
			foreach ( $job_search_terms as $term ) {
				$xS = explode(':', $term);
				$keys = explode(',', $xS[0]);
				$values = explode(',', $xS[1]);
				$all = null;
				// if the "all" keyword is used, get all columns from the view to iterate through
				if ( $keys[0] == 'all' ) {
					$new_keys = array();
					$sql = "SHOW COLUMNS FROM jobs_view";
					$query = $this->db->query($sql);
					if ( $query->num_rows() > 0 ) {
						foreach ( $query->result() as $result ) {
							$new_keys[] = $result->Field;
						}
					}
					$keys = $new_keys;
					$all = true;
				}
				//loop through search keys and values and add them to the query as like and or like statements
				$i=0;
				foreach ( $keys as $key ) {
					$key = str_replace('"', '', $key);
					foreach ( $values as $value ) {
						$key = $all ? strtolower($key) : $db_fields[strtolower($key)];
						if ( $i == 0 ) {
							$this->db->like($key, strtolower($value));
						} else {
							$this->db->or_like($key, strtolower($value));
						}
						$i++;
					}
				}
			}
		}else{
			$this->db->where('status','Active');
		}
		
        return $this->db->from('jobs_view')->count_all_results();
    }
    
	function getHoursPerJob($job_id) {
		return $this->db->select('sum(tasks.total) as total, tasks.job_id')->where('job_id', $job_id)->group_by('job_id')->get("tasks")->row();
	}

	function getPaginatedJobs($limit, $offset = 0, $job_search_terms=null) {
		/* start search terms block */
		//if there are search terms, then loop through and set them to keys and values array
		if (!empty($job_search_terms)){
			//map header field names to db field names and to themselves for recursive queries
			$db_fields = array(
				'job' => 'job_id',
				'name' => 'job_name',
				'owner' => 'owner',
				'status' => 'status',
				'category' => 'category',
				'vehicle' => 'vehicle',
				'effort code' => 'effort_code',
				'business unit' => 'business_unit',
				'hit date' => 'hit_date',
				'region' => 'marketing_region',
				'job_id' => 'job_id',
				'job_name' => 'job_name',
				'effort_code' => 'effort_code',
				'business_unit' => 'business_unit',
				'hit_date' => 'hit_date',
				'marketing_region' => 'marketing_region'
			);
			foreach ( $job_search_terms as $term ) {
				$xS = explode(':', $term);
				$keys = explode(',', $xS[0]);
				$values = explode(',', $xS[1]);
				$all = null;
				// if the "all" keyword is used, get all columns from the view to iterate through
				if ( $keys[0] == 'all' ) {
					$new_keys = array();
					$sql = "SHOW COLUMNS FROM jobs_view";
					$query = $this->db->query($sql);
					if ( $query->num_rows() > 0 ) {
						foreach ( $query->result() as $result ) {
							$new_keys[] = $result->Field;
						}
					}
					$keys = $new_keys;
					$all = true;
				}
				//loop through search keys and values and add them to the query as like and or like statements
				$i=0;
				foreach ( $keys as $key ) {
					$key = str_replace('"', '', $key);
					foreach ( $values as $value ) {
						$key = $all ? strtolower($key) : $db_fields[strtolower($key)];
						if ( $i == 0 ) {
							$this->db->like($key, strtolower($value));
						} else {
							$this->db->or_like($key, strtolower($value));
						}
						$i++;
					}
				}
			}
		}else{
			$this->db->where('status','Active');
		}
/* end search terms block */
		$this->db->order_by("job_id", "desc");
		return $this->db->limit($limit, $offset)->get("jobs_view")->result();
	}
	
	function addJob($data) {	
		$query = array(
			'job_name' => $this->input->post('job_name'),
			'status_id' => $this->input->post('status_id'),
			'job_type_id' => $this->input->post('job_type_id'),
			'marketing_region_id' => $this->input->post('marketing_region_id'),
			'business_unit_id' => $this->input->post('business_unit_id'),
			'effort_code' => $this->input->post('effort_code'),
			'category_id' => $this->input->post('category_id'),
			'tier_id' => $this->input->post('tier_id'),
			'mp_display' => $this->input->post('mp_display'),
			'mp_description' => $this->input->post('mp_description'),
			'mp_search' => $this->input->post('mp_search'),
			'vehicle_id' => $this->input->post('vehicle_id'),
			'hit_date' => $this->input->post('hit_date'),
			'notes' => $this->input->post('notes'),
			'owner' => $this->input->post('owner'),
			'created' => date('Y-m-d H:i:s', now()),
			'created_by' => $this->session->userdata('user_id')
		);
				
		$this->db->insert('jobs', $query);		
	}
	
	function duplicateJob($job_id) {	
		
		$query = $this->db->get_where('jobs', array('job_id' => $job_id));		
		$duplicate = $query->row();
		
		$query = array(
			'job_name' => $duplicate->job_name,
			'status_id' => $duplicate->status_id,
			'user_id' => $duplicate->user_id,
			'job_type_id' => $duplicate->job_type_id,
			'marketing_region_id' => $duplicate->marketing_region_id,
			'business_unit_id' => $duplicate->business_unit_id,
			'effort_code' => $duplicate->effort_code,
			'category_id' => $duplicate->category_id,
			'tier_id' => $duplicate->tier_id,
			'mp_display' => $duplicate->mp_display,
			'mp_description' => $duplicate->mp_description,
			'mp_search' => $duplicate->mp_search,
			'vehicle_id' => $duplicate->vehicle_id,
			'notes' => $duplicate->notes,
			'owner' => $duplicate->owner,
			'created' => date('Y-m-d H:i:s', now()),
			'created_by' => $this->session->userdata('user_id')
		);
			
		$this->db->insert('jobs', $query);	
		$job_id = $this->db->insert_id();	
		
		return $job_id;
	}
	
	function getJob($job_id) {	
		$this->db->select('jobs.*, rate_cards.*, user_admin.users_view.name as created_by');
		$this->db->join('rate_cards','rate_cards.business_unit_id = jobs.business_unit_id and rate_cards.vehicle_id = jobs.vehicle_id and rate_cards.tier_id = jobs.tier_id', 'left');
		$this->db->join('user_admin.users_view','user_admin.users_view.user_id = jobs.created_by', 'left');
		return $this->db->get_where('jobs', array('job_id' => $job_id))->row();		
	}
	
	function getJobView($job_id) {
		return $this->db->get_where('jobs_view', array('job_id' => $job_id))->row(); 
	}
	
	function updateJob($data) {
		$job_id = $this->input->post('job_id');
		
		$query = array(
			'job_name' => $this->input->post('job_name'),
			'status_id' => $this->input->post('status_id'),
			'job_type_id' => $this->input->post('job_type_id'),
			'marketing_region_id' => $this->input->post('marketing_region_id'),
			'business_unit_id' => $this->input->post('business_unit_id'),
			'effort_code' => $this->input->post('effort_code'),
			'category_id' => $this->input->post('category_id'),
			'tier_id' => $this->input->post('tier_id'),
			'mp_display' => $this->input->post('mp_display'),
			'mp_description' => $this->input->post('mp_description'),
			'mp_search' => $this->input->post('mp_search'),
			'vehicle_id' => $this->input->post('vehicle_id'),
			'hit_date' => $this->input->post('hit_date'),
			'notes' => $this->input->post('notes'),
			'owner' => $this->input->post('owner'),
			'modified_by' => $this->session->userdata('user_id')
		);
		
		$this->db->where('job_id', $job_id)->update('jobs',$query);				
	}
	
	function getStatusDropdown() {
		$query = $this->db->get('statuses');	
							
		foreach ($query->result_array() as $row){
			$data[$row['status_id']] = $row['status'];
		}
				
		return $data;
	}
	
	function updateStatus($status_id, $job_id) {
		$this->db->where('job_id', $job_id);
		
		$query = array(
			'status_id' => $status_id
		);
		
		$this->db->update('jobs', $query);
	}
	
	function getTypeDropdown() {
		$query = $this->db->get('job_types');		
		
		$data = array(' ' => '--Select a Type--');
		
		foreach ($query->result_array() as $row){
			$data[$row['job_type_id']] = $row['job_type'];
		}
		
		return $data;
	}
	
	function getRegionsDropdown() {
		$query = $this->db->get('marketing_regions');		
		
		$data = array(' ' => '--Select a Region--');
				
		foreach ($query->result_array() as $row){
			$data[$row['marketing_region_id']] = $row['marketing_region'];
		}
		
		return $data;
	}
	
	function getTierDropdown() {
		$query = $this->db->get('tiers');		
		
		$data = array(' ' => '--Select a Tier--');
		
		foreach ($query->result_array() as $row){
			$data[$row['tier_id']] = $row['tier'];
		}
		
		return $data;
	}
}