<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

	function __construct() {
		
	}
	function getUser($user_id) {
		$this->db->where('user_id', $user_id);
		$query = $this->db->get("user_admin.users_view");
		return $query->row();
	}
	
	function getSubordinants($user_id) {
		$this->db->where('manager_id', $user_id);
		$query = $this->db->get("user_admin.users");
		return $query->result();
	}
	
	function getOwnerDropdown() {
		$this->db->order_by("first_name", "asc");
		$query = $this->db->get('user_admin.users');	
							
		$data = array(' ' => '--Select a User--');
							
		foreach ($query->result_array() as $row){
			$data[$row['user_id']] = $row['first_name'] . ' ' . $row['last_name'] ;
		}
				
		return $data;
	}
}