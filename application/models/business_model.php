<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Business_model extends CI_Model {

	function __construct() {
		
	}
	
	function selectAllUnits() {
		return $this->db->get("business_units")->result();
	}
	
	function countUnits(){
        return $this->db->count_all('business_units');
    }

                                                                                                                                        	
	function getPaginatedUnits($limit, $offset = 0) {
		return $this->db->order_by('business_unit')->limit($limit, $offset)->get("business_units_view")->result();
	}
	
	function getBusinessUnit($business_unit_id) {				
		return $this->db->get_where('business_units', array('business_unit_id' => $business_unit_id))->row();		
	}
	
	function updateBusinessUnit($data){
		$business_unit_id = $this->input->post('business_unit_id');
		
		$category_id = $this->input->post('category_id');
		$vehicle_id = $this->input->post('vehicle_id');
		
		$select = 'count(1) as counted';
		
		$where = array('business_unit_id'=>$business_unit_id);
		
		if($category_id){
			//query the db to check if there is a category for this business unit
			$where['category_id'] = $category_id;
			$category_count = $this->db->select($select)->where($where)->limit(1)->get("categories_catalog")->row()->counted == 0 ? true : null;
		
			//query the db to check if there is an inactive category for this business unit
			$where['active'] = 0;
			$category_active = $this->db->select($select)->where($where)->get("categories_catalog")->row()->counted > 0 ? true : null;
			
			//if there is not already this category for this business unit, then insert
			!$category_count || $this->db->insert('categories_catalog', array('business_unit_id'=>$business_unit_id,'category_id'=>$category_id));
			unset($where['active']);
			
			//if this category is inactive for this business unit, then make it active
			!$category_active || $this->db->where($where)->update('categories_catalog', array('active'=>1));
			unset($where['category_id']);
		}
		if($vehicle_id){
			//query the db to check if there is a vehicle for this business unit
			$where['vehicle_id'] = $vehicle_id;
			$vehicle_count = $this->db->select($select)->where($where)->limit(1)->get("vehicles_catalog")->row()->counted == 0 ? true : null;
		
			//query the db to check if there is an inactive category for this business unit
			$where['active'] = 0;
			$vehicle_active = $this->db->select($select)->where($where)->get("vehicles_catalog")->row()->counted > 0 ? true : null;
			
			//if there is not already this vehicle for this business unit, then insert
			!$vehicle_count || $this->db->insert('vehicles_catalog', array('business_unit_id'=>$business_unit_id,'vehicle_id'=>$vehicle_id));
			unset($where['active']);
			
			//if this vehicle is inactive for this business unit, then make it active
			!$vehicle_active || $this->db->where($where)->update('vehicles_catalog', array('active'=>1));
			unset($where['vehicle_id']);
		}
	}
	
	function getBuDropdown() {
		
		$query = $this->db->order_by('business_unit')->get('business_units');		
		
		$data = array(' ' => '--Select a Business Unit--');
		
		foreach ($query->result_array() as $row){
			$data[$row['business_unit_id']] = $row['business_unit'];
		}
		
		return $data;
	}
	
	function getCategoryDropdown($business_unit_id=null) {		
		if($business_unit_id){
			$join = 'categories_catalog.category_id = categories.category_id';
			$where = array('business_unit_id'=>$business_unit_id,'active'=>1);
			$query = $this->db->join('categories_catalog', $join)->where($where)->order_by('category')->get('categories');
			
			$categories[0] = '--Select a Category--';		
			foreach ($query->result_array() as $row){
				$categories[$row['category_id']] = $row['category'];
			}
		}else{
			$categories = array(' ' => '--Select a Business Unit Above--');
		}
		return $categories;
	}
	
	function getCategoryUnitDropdown() {
		$query = $this->db->order_by('category')->get('categories');
		$data = array(' ' => '--Select a Category--');
		foreach ($query->result_array() as $row){
			$data[$row['category_id']] = $row['category'];
		}
		return $data;
	}
	
	function getCategories($business_unit_id=null) {		
		$select = 'categories.*, categories_catalog.*';
		$join = 'categories_catalog.category_id = categories.category_id';
		$where = array('active'=>1);
		!$business_unit_id || $where['business_unit_id'] = $business_unit_id;
		return $this->db->select($select)->join('categories_catalog', $join)->where($where)->order_by('category')->get("categories")->result();
	}
	
	function getVehicleDropdown($business_unit_id=null) {		
		if($business_unit_id){
			$join = 'vehicles_catalog.vehicle_id = vehicles.vehicle_id';
			$where = array('business_unit_id'=>$business_unit_id,'active'=>1);
			$query = $this->db->join('vehicles_catalog', $join)->where($where)->order_by('vehicle')->get('vehicles');
			$vehicles[0] = '--Select a Vehicle--';		
			foreach ($query->result_array() as $row){
				$vehicles[$row['vehicle_id']] = $row['vehicle'];
			}
		}else{
			$vehicles = array(' ' => '--Select a Business Unit Above--');
		}
		return $vehicles;
	}
	
	function getVehicleUnitDropdown() {
		$query = $this->db->order_by('vehicle')->get('vehicles');
		$data = array(' ' => '--Select a Vehicle--');
		foreach ($query->result_array() as $row){
			$data[$row['vehicle_id']] = $row['vehicle'];
		}
		return $data;
	}
	
	function getVehicles($business_unit_id=null) {		
		$select = 'vehicles.*, vehicles_catalog.*';
		$join = 'vehicles_catalog.vehicle_id = vehicles.vehicle_id';
		$where = array('active'=>1);
		!$business_unit_id || $where['business_unit_id'] = $business_unit_id;
		return $this->db->select($select)->join('vehicles_catalog', $join)->where($where)->order_by('vehicle')->get("vehicles")->result();
	}
	
	function removeCategory($catalog_id){
		$this->db->where('catalog_id', $catalog_id)->update('categories_catalog', array('active'=>0));
	}
	
	function removeVehicle($catalog_id){
		$this->db->where('catalog_id', $catalog_id)->update('vehicles_catalog', array('active'=>0));
	}
	
}