<div class="row">	
	<table class="table table-striped tablesorter">
		<thead class="thead">
			<th>Business Unit</th>
			<th>Categories</th>
			<th>Vehicles</th>
			<td class="center">Update</td>
		</thead>
		<tbody>
		<?php foreach ($units as $unit) { ?>
			<tr>
				<td><?= $unit->business_unit ?></td>
				<td><?= $unit->category_count ?> Categories</a></td>
				<td><?= $unit->vehicle_count ?> Vehicles</a></td>
				<td class="center"><a href="<?=base_url()?>business/update/<?= $unit->business_unit_id ?>">Update</a></td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
	<div class="pagination">
		<?= $links ?> 
	</div>
</div>