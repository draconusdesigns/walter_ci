<div class="row">
	<div class="col-md-4 pull-right">	
		<a href="<?= base_url() ?>business/" type="button" class="btn btn-primary pull-right" role="button">
			<i class='fa  fa-arrow-circle-left'></i> Return to Business Units
		</a>
	</div>
	<div class="col-md-12">	
		<form class="form-horizontal" method="post" enctype="multipart/form-data">
			<input type="hidden" name="business_unit_id" value="<?= isset($unit->business_unit_id) ? $unit->business_unit_id : set_value("business_unit_id") ?>" />

			<div class="form-group col-md-3">
				<div class="col-md-12">
					<label for="category_id" class="control-label">Add a Category:</label><br>
					<?php $category_id = set_value("category_id"); ?>					
					<?= form_dropdown('category_id', $allCategories, $category_id, 'class="form-control"') ?>
				</div>	
			</div>
		
			<div class="form-group col-md-3">
				<div class="col-md-12">
					<label for="vehicle_id" class="control-label">Add a Vehicle:</label><br>
					<?php $vehicle_id = set_value("vehicle_id"); ?>					
					<?= form_dropdown('vehicle_id', $allVehicles, $vehicle_id, 'class="form-control"') ?>
				</div>
			</div>
			<div class="form-group col-md-3">
				<br>
				<input class="btn btn-default inline-submit" type="submit" value="Submit" />		
			</div>
		</form>
	</div>
	<div class="col-md-6">	
		<h3>Categories</h3>
		<table class="table table-striped tablesorter">
			<thead class="thead">
				<th>Name</th>
				<td class="center">Remove</td>
			</thead>
			<tbody>	
			<?php foreach ($categories as $category) { ?>
				<tr>
					<td><?= $category->category ?></td>
					<td class="center"><a href='<?= base_url() ?>business/remove_category/<?= $category->catalog_id ?>/<?=$unit->business_unit_id ?>'>Remove</a></td>
					
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>	
	
	<div class="col-md-6">	
		<h3>Vehicles</h3>
		<table class="table table-striped tablesorter">
			<thead class="thead">
				<th>Name</th>
				<td class="center">Remove</td>
			</thead>
			<tbody>
			<?php foreach ($vehicles as $vehicle) { ?>
				<tr>
					<td><?= $vehicle->vehicle ?></td>
					<td class="center"><a href='<?= base_url() ?>business/remove_vehicle/<?= $vehicle->vehicle_id ?>/<?=$unit->business_unit_id ?>'>Remove</a></td>
					
				</tr>
			<?php } ?>	
			</tbody>
		</table>
	</div>
</div>