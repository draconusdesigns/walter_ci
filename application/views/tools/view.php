<div class="row">
	<div class="col-md-7 dashboard">	
		<h3><i class='fa fa-clock-o'></i> Timesheet statuses for</h3>
		<table class="table table-striped tablesorter">
			<thead class="thead">
				<th>Week Ending</th>
				<th>Timesheet Status</th>
				<td class="center">Total Time</td>
			</thead>
			<tbody>

			<?php foreach ($timesheets as $timesheet) { ?>
				<tr class="">
					<td><a href="<?= base_url() ?>timesheets/update/<?= $timesheet->timesheet_id ?>"><?= $timesheet->week_ending ?></a></td>
				
					<td><?= $timesheet->Total ?></td>
					<td class="center"><?= $timesheet->status ?></td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
	<div class="col-md-4 dashboard pull-right">
		<h3><i class='fa fa-calendar'></i> Benefits time for</h3>
		<table class="table table-striped tablesorter">
			<thead class="thead">
				<th>Type</th>
				<th>Hours</th>
				<th>Days</th>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
</div>