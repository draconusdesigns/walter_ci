<?php if($timesheets):?>
<div class="row">
		<div class="col-md-7 dashboard">	
		<h3><i class='fa fa-clock-o'></i> Timesheet statuses for</h3>

			<table class="table table-bordered tablesorter">
				<thead class="thead">
					<th>Week Ending </th>	
					<th>Total Time</th>	
					<th>Completed</th>
				</thead>
				<tbody>						
					<?php foreach ($timesheets as $timesheet) { ?>
						<tr class="">
							<td><a href="<?= base_url() ?>timesheets/view/<?= $timesheet->timesheet_id ?>"><?= $timesheet->week_ending ?></a></td>

							<td><?= isset($timesheet->total)? $timesheet->total : '0.00'; ?></td>
							<td class="center"><?= $timesheet->status ?></td>
						</tr>
					<?php } ?>
				</tbody>	
			</table>
			<div class="pagination">
				<?= $links ?> 
			</div>
		</div>	

<div class="col-md-4 dashboard pull-right">
		<h3><i class='fa fa-calendar'></i> Benefits time for</h3>
		<table class="table table-striped tablesorter">
			<thead class="thead">
				<th>Type</th>
				<th>Hours</th>
				<th>Days</th>
			</thead>
			<tbody>
				<?php foreach ($nonBillable as $each) { ?>
					<tr class="">
						<td><?= $each->task_name ?></td>
						<td><?= $each->total ?></td>
						<td><?= $each->days ?></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>
</div>

<?php else:?>
	<tr>
		<td colspan="4">
			<div class="alert alert-dismissable alert-danger">
				No Timesheets are Currently Attached to this user.
			</div>
		</td>
	</tr>
<?php endif;?>