<div class="row">
	<table id="search-results" class="table table-striped tablesorter">
        <thead class="thead">
        	<th>Direct Reports</th>
			<td class="center">View</td>
        </thead>
        <tbody id="results-body">
<?php if($subs):?>
	<?php foreach ($subs as $sub): ?>
		<tr>
			<td><?= $sub->first_name ?> <?= $sub->last_name ?></td>
			<td class="center"><a href="<?= base_url()?>tools/timesheets/<?= $sub->user_id ?>">View Timesheets</a></td>
		</tr>
    <?php endforeach; ?>
<?php else: ?>
		<tr>
			<td colspan="2">
				<div class="alert alert-dismissable alert-danger">
					You currently have no listed subordinants.
				</div>
			</td>
		</tr>
<?php endif; ?>
		</tbody>
	</table>
</div>
