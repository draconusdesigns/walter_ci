<script type="text/javascript" src="<?= asset_url() ?>/js/ratecards.js"></script>

<?php if(validation_errors()):?>
	<div class="alert alert-dismissable alert-danger">
	  <button type="button" class="close" data-dismiss="alert">×</button>
	  <?= validation_errors() ?>
	</div>
<?php endif;?>

<form class="form-horizontal" method="post" enctype="multipart/form-data">
	<input type="hidden" name="rate_card_id" value="<?= isset($ratecard->rate_card_id) ? $ratecard->rate_card_id : set_value("rate_card_id") ?>" />
	
	<div class="form-group">
		<label for="marketing_region_id" class="col-md-3 control-label required">Market Region: </label>
		<div class="col-md-5">
			<?php $marketing_region_id = isset($ratecard->marketing_region_id) ? $ratecard->marketing_region_id : set_value("marketing_region_id"); ?>
			<?= form_dropdown('marketing_region_id', $marketingRegions, $marketing_region_id, 'class="form-control"') ?>
		</div>	
	</div>
	
	<div class="form-group">
		<label for="business_unit_id" class="col-md-3 control-label required">Business Unit: </label>
		<div class="col-md-5">
			<?php $business_unit_id = isset($ratecard->business_unit_id) ? $ratecard->business_unit_id : set_value("business_unit_id"); ?>
			<?= form_dropdown('business_unit_id', $businessUnits, $business_unit_id, 'class="form-control" id="business_unit_id"') ?>
		</div>	
	</div>
	
	<div class="form-group">
		<label for="vehicle_id" class="col-md-3 control-label required">Vehicle: </label>
		<div class="col-md-5">
			<?php $vehicle_id = isset($ratecard->vehicle_id) ? $ratecard->vehicle_id : set_value("vehicle_id"); ?>
			<?= form_dropdown('vehicle_id', $vehicles, $vehicle_id, 'class="form-control" id="vehicle_id"') ?>
		</div>
	</div>
	
	<div class="form-group">
		<label for="tier_id" class="col-md-3 control-label required">Tier: </label>
		<div class="col-md-5">
			<?php $tier_id = isset($ratecard->tier_id) ? $ratecard->tier_id : set_value("tier_id"); ?>
			<?= form_dropdown('tier_id', $tiers, $tier_id, 'class="form-control"') ?>
		</div>	
	</div>
	
	<div class="form-group">
		<label for="rate" class="col-md-3 control-label required">Average Hours: </label>
		<div class="col-md-5">
			<input type="text" name="rate" class="input-xxlarge form-control" value="<?= isset($ratecard->rate) ? $ratecard->rate : set_value("rate") ?>"/>
		</div>	
	</div>
	
	<div class="col-xs-3 col-sm-1 col-md-4 col-md-offset-4">
		<input class="btn btn-default pull-right" type="submit" value="submit" />
	</div>
</form>