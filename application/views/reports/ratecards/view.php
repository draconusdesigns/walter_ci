<div class="row">
	<div class="col-md-4 pull-right">	
		<a href="<?= base_url() ?>reports/ratecards" type="button" class="btn btn-primary pull-right" role="button">
			<i class='fa  fa-arrow-circle-left'></i> Return to Ratecards
		</a>
	</div>

	<div class="col-md-12">
		<div class="table_container">		
			<table class="table table-bordered">
				<tbody>	
					<tr>
						<td class="right bold">Region: </td>
						<td><?= $ratecard->marketing_region ?></td>		
					</tr>
					<tr>
						<td class="right bold">Business Unit: </td>
						<td><?= $ratecard->business_unit ?></td>		
					</tr>
					<tr>
						<td class="right bold">Vehicle: </td>
						<td><?= $ratecard->vehicle ?></td>		
					</tr>
					<tr>
						<td class="right bold">Tier: </td>
						<td><?= $ratecard->tier ?></td>		
					</tr>
					<tr>
						<td class="right bold">Average Hours: </td>
						<td><?= $ratecard->rate ?></td>		
					</tr>				
				</tbody>	
			</table>
		</div>
	</div>
</div>