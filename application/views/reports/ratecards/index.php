<div class="row">
	<div class="col-md-4 pull-right">	
		<a href="<?= base_url() ?>reports/add_ratecard" type="button" class="btn btn-primary pull-right" role="button">
			<i class="fa fa-plus-circle"></i> Add a new Ratecard
		</a>
	</div>
	
	<div class="col-md-12">
		<table class="table table-striped tablesorter">
			<thead class="thead">
				<th>Region</th>
				<th>Business Unit</th>
				<th>Vehicle</th>
				<th>Tier</th>	
				<th>Average Hours</th>											
				<td class="center"><i class='fa fa-edit'></i></td>
				<td class="center"><i class='fa fa-eye'></i></td>		
			</thead>
			<tbody>
			<?php foreach ($allRatecards as $ratecard) { ?>
				<tr class="">
					<td><?= $ratecard->marketing_region ?></td>
					<td><?= $ratecard->business_unit ?></td>
					<td><?= $ratecard->vehicle ?></td>
					<td><?= $ratecard->tier ?></td>
					<td><?= $ratecard->rate ?></td>
					<td class="center"><a href="<?=base_url()?>reports/edit_ratecard/<?=$ratecard->rate_card_id?>">Edit</a></td>
					<td class="center"><a href="<?=base_url()?>reports/view_ratecard/<?=$ratecard->rate_card_id?>">View</a></td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
		<div class="pagination">
			<?= $links ?> 
		</div>
	</div>
</div>