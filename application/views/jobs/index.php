<script type="text/javascript" src="<?= asset_url() ?>/js/jobsearch.js"></script>

<div class="row">
	<div class="col-md-8">
	    <form class="form-block" name="last_timesheet_selector" id="last_timesheet_selector" action="">
	        <select name="search-term" id="search-term" class="form-control pull-left" style="width:auto;margin-right:10px;">
	            <option value='all'>All</option>
	        </select>
	        <input type="text" name="search-terms" id="search-terms" placeholder="Search Terms" autocomplete="off" class="form-control pull-left" style="width:250px;" value=""/>
	        <button class="btn-primary form-control pull-left" id="search-btn" style="width:auto;margin-left:10px;">Search</button>
	        <img src="<?= base_url() ?>assets/css/ajax-loader.gif" style="margin-left:10px;margin-top:16px;display:none;" id="loader-icon" />
	    </form>
			<a style="text-decoration: none;" href="<?= base_url() ?>jobs/search_reset">
				<button class="btn-danger btn-xs pull-left" style="width:auto;margin-left:10px;">Reset Search</button>
			</a>
	</div>	
<?php if($is_admin || $is_ae): ?>
	<div class="col-md-4 pull-right">	
		<a href="<?= base_url() ?>jobs/add" type="button" class="btn btn-primary pull-right" role="button">
			<i class='fa fa-plus-circle'></i> Create new job
		</a>
	</div>
<?php endif; ?>
</div>
<?php if ($job_search_terms): ?>
	<?php foreach ( $job_search_terms as $term ): ?>
		<?php
			$xS = explode(':', $term);
			$keys = $xS[0];
			$vals = $xS[1];
		?>
		<div class='alert alert-info alert-dismissible filter pull-left' role='alert'>
			<button type='button' class='close rem-term' data-dismiss='alert' data-rel="<?= $term ?>">
				<span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span>
			</button>
			<strong><?= ucwords($keys) ?></strong> : <?= $vals ?>
		</div>
	<?php endforeach; ?>
<?php endif;?>
<div class="row">
	    <table id="search-results" class="table table-striped tablesorter">
	        <thead class="thead">
	        	<th class="center">Job</th>
		        <th class="center">Effort Code</th>
		        <th>Status</th>
		        <th>Name</th>
		        <th>Region</th>
		        <th>Business Unit</th>
		        <th>Owner</th>
		        <th>Hit Date</th>
				<td class="center">Add to TS</td>
	        </thead>
	        <tbody id="results-body">
		<?php if($allJobs): ?>
	        <?php foreach ($allJobs as $job) { ?>
	            <tr>
	                <td class="center">
						<?php if($is_admin || $is_ae): ?>
							<a href="<?=base_url()?>jobs/edit/<?=$job->job_id?>"><?= $job->job_id ?></a>
						<?php else: ?>
							<a href="<?=base_url()?>jobs/view/<?=$job->job_id?>"><?= $job->job_id ?></a>
						<?php endif;?>
					</td>	
	                <td class="center"><?= $job->effort_code ?></td>
	                <td>
						<?php if($is_admin_or_manager): ?>
							<?php $status_id = isset($job->status_id) ? $job->status_id : set_value("status_id"); ?>
	                    	<?= form_dropdown('status_id', $statuses, $status_id, "id='{$job->job_id}' class='status_update'") ?>
						<?php else: ?>
							<?= $job->status ?>
						<?php endif;?>
					</td>
	                <td><?= $job->job_name ?></td>
		            <td><?= $job->marketing_region ?></td>
	                <td><?= $job->business_unit ?></td>
	                <td><?= $job->owner ?></td>
	                <td><?= $job->hit_date ?></td>
					<td class="center">
						<?php if($job->status == "Active"):?>
							<a title="Add to Current Timesheet" href="<?=base_url()?>jobs/addToTimesheet/<?=$job->job_id?>"><i class='fa fa-plus-circle'></i></a>
						<?php endif; ?>
					</td>
	            </tr>
	        <?php } ?>
		<?php else: ?>
			<tr>
				<td colspan="10">
					<div class="alert alert-dismissable alert-danger">
						There are no results in the current view.
					</div>
				</td>
			</tr>
		<?php endif; ?>
	        </tbody>
	    </table>
	    <div class="pagination">
	        <?= $links ?>
	    </div>
</div>