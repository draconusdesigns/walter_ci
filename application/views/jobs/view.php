<div class="row">
	<div class="col-md-4 pull-right">	
		<a href="<?= base_url() ?>jobs" type="button" class="btn btn-primary pull-right" role="button">
			<i class='fa  fa-arrow-circle-left'></i> Return to Jobs
		</a>
	</div>

	<div class="col-md-12">
		<div class="table_container">
			<table class="table table-bordered">
				<tbody>	
					<tr>
						<td class="right bold">Job Name: </td>
						<td><?= isset($job->job_name)? $job->job_name : ''; ?></td>		
					</tr>
					<tr>
						<td class="right bold">Job Status: </td>
						<td><?= isset($job->status)? $job->status : ''; ?></td>
					</tr>
					<tr>
						<td class="right bold">Job Owner:</td>
						<td><?= isset($job->owner)? $job->owner : ''; ?></td>
					</tr>
					<tr>
						<td class="right bold">Job Type: </td>
						<td><?= isset($job->job_type)? $job->job_type : ''; ?></td>
					</tr>	
					<tr>
						<td class="right bold">Market Region: </td>
						<td><?= isset($job->marketing_region)? $job->marketing_region : ''; ?></td>
					</tr>	
					<tr>
						<td class="right bold">Business Unit: </td>
						<td><?= isset($job->business_unit)? $job->business_unit : ''; ?></td>
					</tr>
				<?php if($job->business_unit == 'Quill'): ?>
					<tr>
						<td class="right bold">Effort Code: </td>
						<td><?= isset($job->effort_code)? $job->effort_code : ''; ?></td>
					</tr>
				 <?php endif;?>	
					<tr>
						<td class="right bold">Category: </td>
						<td><?= isset($job->category)? $job->category : ''; ?></td>
					</tr>	
					<tr>
						<td class="right bold">Vehicle:</td>
						<td><?= isset($job->vehicle)? $job->vehicle : ''; ?></td>
					</tr>
					<tr>
						<td class="right bold">Tier:  </td>
						<td><?= isset($job->tier)? $job->tier : ''; ?></td>
					</tr>	
					<tr>
						<td class="right bold">Display on Marketing Portal?:</td>
						<td><?php if(isset($job->mp_display) && $job->mp_display == true):?>Yes<?php else:?>No<?php endif;?></td>
					</tr>	
					<tr>
						<td class="right bold">Marketing Portal Description:</td>
						<td><?= isset($job->mp_description)? $job->mp_description : ''; ?></td>
					</tr>
					<tr>
						<td class="right bold">Portal Search Keywords:</td>
						<td><?= isset($job->mp_search)? $job->mp_search: ''; ?></td>
					</tr>
					<tr>
						<td class="right bold">Hit Date: </td>
						<td><?= isset($job->hit_date)? $job->hit_date: ''; ?></td>
					</tr>
					<tr>
						<td class="right bold">Job Notes: </td>
						<td><?= isset($job->notes)? $job->notes: ''; ?></td>
					</tr>
					<tr>
						<td class="right bold">Estimated Completion Time: </td>
						<td><?= isset($job->rate) ? $job->rate :"No estimate available"; ?></td>
					</tr>
					<tr>
						<td class="right bold">Current Hours put to Job: </td>
						<td><?= isset($job_hours->total)? $job_hours->total: ''; ?></td>
					</tr>
					
					<?php  $servername = 'advserver.staples.com/Departments/'; ?>
					
					<?php if(isset($job->job_type) && $job->job_type == 'Online'): ?>
							<?php $servername = 'interactive.staples.com/Projects/'; ?>
					<?php endif;?>	
					 
					<?php $enclosing_folder_path =  ('afp://' . $servername . 'Jobs/' . $job->marketing_region . '/' .  $job->business_unit  . '/' .  $job->category   . '/' .  $job->vehicle); ?>	
					<?php $full_folder_path =  ($enclosing_folder_path . '/' . $job->job_id . '_' . substr($job->hit_date, 0,4) . '_' . $job->job_name);  ?>
						
					<tr>
						<td class="right bold">Enclosing Folder Path: </td>
						<td class="right bold"><a href="<?=  $enclosing_folder_path ?>"><?=  $enclosing_folder_path ?></a></td>
						
					</tr>
					
					<tr>
						<td class="right bold">Enclosing Folder Path: </td>
						<td class="right bold"><a href="<?=  $full_folder_path ?>"><?=  $full_folder_path ?></a></td>
						
					</tr>

				</tbody>	
			</table>
		</div>	
	</div>	
</div>