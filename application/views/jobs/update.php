<script type="text/javascript" src="<?= asset_url() ?>/js/jobs.js"></script>

<?php if(validation_errors()):?>
	<div class="alert alert-dismissable alert-danger">
	  <button type="button" class="close" data-dismiss="alert">×</button>
	  <?= validation_errors() ?>
	</div>
<?php endif;?>

<div class="row">
	<form class="form-horizontal" method="post" enctype="multipart/form-data">
		<input type="hidden" name="job_id" value="<?= isset($job->job_id) ? $job->job_id : set_value("job_id") ?>" />
	
		<div class="form-group">
			<label for="job_name" class="col-md-3 control-label required">Job Name: </label>
			<div class="col-md-5">
				<input type="text" name="job_name" class="input-xxlarge form-control" value="<?= isset($job->job_name) ? $job->job_name : set_value("job_name") ?>"/>
			</div>	
		</div>
	
		<div class="form-group">
			<label for="status_id" class="col-md-3 control-label">Job Status: </label>
			<div class="col-md-5">
				<?php $status_id = isset($job->status_id) ? $job->status_id : set_value("status_id"); ?>
				<?= form_dropdown('status_id', $statuses, $status_id, 'class="form-control"') ?>
			</div>	
		</div>
	
		<div class="form-group">
			<label for="owner" class="col-md-3 control-label required">Job Owner: </label>	
			<div class="col-md-5">
				<?php $owner = isset($job->owner) ? $job->owner : false; ?>
				<?= form_dropdown('owner', $users, $owner, 'class="form-control"') ?>
			</div>	
		</div>
	
		<div class="form-group">
			<label for="job_type_id" class="col-md-3 control-label required">Job Type: </label>
			<div class="col-md-5">
				<?php $job_type_id = isset($job->job_type_id) ? $job->job_type_id : set_value("job_type_id"); ?>
				<?= form_dropdown('job_type_id', $jobTypes, $job_type_id, 'class="form-control"') ?>
			</div>	
		</div>
	
		<div class="form-group">
			<label for="marketing_region_id" class="col-md-3 control-label required">Market Region: </label>
			<div class="col-md-5">
				<?php $marketing_region_id = isset($job->marketing_region_id) ? $job->marketing_region_id : set_value("marketing_region_id"); ?>
				<?= form_dropdown('marketing_region_id', $marketingRegions, $marketing_region_id, 'class="form-control"') ?>
			</div>	
		</div>
	
		<div class="form-group">
			<label for="business_unit_id" class="col-md-3 control-label required">Business Unit: </label>
			<div class="col-md-5">
				<?php $business_unit_id = isset($job->business_unit_id) ? $job->business_unit_id : set_value("business_unit_id"); ?>
				<?= form_dropdown('business_unit_id', $businessUnits, $business_unit_id, 'class="form-control" id="business_unit_id"') ?>
			</div>	
		</div>
		
		<div class="form-group">
			<label for="effort_code" id="label_effort_code" class="col-md-3 control-label required">Effort Code: </label>
			<div class="col-md-5">
				<input type="text" id="effort_code" name="effort_code" class="input-xxlarge form-control" value="<?= isset($job->effort_code) ? $job->effort_code : set_value("effort_code") ?>"/>
			</div>	
		</div>
	
		<div class="form-group">
			<label for="category_id" class="col-md-3 control-label required">Category: </label>
			<div class="col-md-5">
				<?php $category_id = isset($job->category_id) ? $job->category_id : set_value("category_id"); ?>
				<?= form_dropdown('category_id', $categories, $category_id, 'class="form-control" id="category_id"') ?>
			</div>	
		</div>
	
		<div class="form-group">
			<label for="vehicle_id" class="col-md-3 control-label required">Vehicle: </label>
			<div class="col-md-5">
				<?php $vehicle_id = isset($job->vehicle_id) ? $job->vehicle_id : set_value("vehicle_id"); ?>
				<?= form_dropdown('vehicle_id', $vehicles, $vehicle_id, 'class="form-control" id="vehicle_id"') ?>
			</div>
		</div>
	
		<div class="form-group">
			<label for="tier_id" class="col-md-3 control-label required">Tier: </label>
			<div class="col-md-5">
				<?php $tier_id = isset($job->tier_id) ? $job->tier_id : set_value("tier_id"); ?>
				<?= form_dropdown('tier_id', $tiers, $tier_id, 'class="form-control"') ?>
			</div>	
		</div>
	
		<div class="form-group">
			<label for="mp_display" class="col-md-3 control-label">Display on Marketing Portal?: </label>
			<div class="checkbox col-md-5">
				<?php $mp_display = (!empty($job->mp_display)) ? true : false; ?>
				<input type="checkbox" name="mp_display" value="1" <?= set_checkbox('mp_display', '1', $mp_display) ?> /> Yes
			</div>
		</div>
	
		<div class="form-group">
			<label for="mp_description" class="col-md-3 control-label">Marketing Portal Description: </label>
			<div class="col-md-5">
				<textarea class="text-left" rows="3" cols="32" type="text" name="mp_description" id="mp_description"><?= isset($job->mp_description) ? $job->mp_description : set_value("mp_description") ?></textarea>
			</div>	
		</div>
	
		<div class="form-group">
			<label for="mp_search" class="col-md-3 control-label">Portal Search Keywords: </label>
			<div class="col-md-5">
				<textarea class="text-left" rows="3" cols="32" type="text" name="mp_search" id="mp_search"><?= isset($job->mp_search) ? $job->mp_search : set_value("mp_search") ?></textarea>
			</div>	
		</div>
		
		<div class="form-group">
			<div class="input-daterange">
				<label for="hit_date" class="col-md-3 control-label">Hit Date: </label>
				<div class="col-md-5">
					<div class="input-group date" id="datepicker_hit_date" data-date="" data-date-format="yyyy-mm-dd">
					      <input name="hit_date" class="form-control" type="text" value="<?= isset($job->hit_date) ? $job->hit_date : set_value("hit_date") ?>">
					      <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					    </div>			
				</div>	
			</div>
		</div>
	
		<div class="form-group">
			<label for="notes" class="col-md-3 control-label">Job Notes: </label>
			<div class="col-md-5">
				<textarea class="text-left" rows="3" cols="32" type="text" name="notes" id="notes"><?= isset($job->notes) ? $job->notes : set_value("notes") ?></textarea>
			</div>	
		</div>
	
		<div class="form-group">
			<label for="rate_card" class="col-md-3 control-label">Estimated Completion Time: </label>
			<div class="col-md-5">
				<input name="rate_card" class="form-control" type="text" value="<?= isset($job->rate) ? $job->rate :"No estimate available"; ?>" disabled>
			</div>	
		</div>
	
		<div class="form-group">
			<label for="rate_card" class="col-md-3 control-label">Hours Put to Job: </label>
			<div class="col-md-5">
				<input name="rate_card" class="form-control" type="text" value="<?= isset($job_hours->total) ? $job_hours->total :"No hours for this job"; ?>" disabled>
			</div>	
		</div>
	
		<?php $colwidth_sub = 'col-md-7' ?>
			<?php if(isset($job->job_id)): ?>
				<?php 
					$colwidth_sub = 'col-md-5';
				?>	
			<?php endif; ?>

		<div class=" col-xs-3 <?= $colwidth_sub ?> ">
			<input class="btn btn-primary pull-right" type="submit" value="submit" />
		</div>
		<?php if(isset($job->job_id)): ?>
		<form action="<?= base_url() ?>jobs/duplicate" class="form-horizontal" method="post" enctype="multipart/form-data">
			<div class="col-md-2">
			
				<input type="hidden" name="job_id" value="<?= isset($job->job_id) ? $job->job_id : set_value("job_id") ?>" />
				<input class="btn btn-default pull-right" type="submit" value="Create Duplicate Job" />
			</div>
		</form>
	<?php endif; ?>
		<div class=" col-xs-3 col-md-1 ">
			<button class="btn btn-default pull-right" type="button" value="cancel">Cancel</button>
		</div>
	</form>
</div>	
<?php if(isset($job->created)):?>
	<div class="row">
				<br><br>
		 <div class="col-md-offset-3 col-md-5 panel panel-default">
			<p><strong>Created:</strong> <?= isset($job->created) ? date('M j, Y @ g:ia', strtotime($job->created)) :"" ?></p>
			<p><strong>by</strong> <?= isset($job->created_by) ? $job->created_by :"" ?></p>
		 	<p><strong>Last Modified:</strong> <?= isset($job->modified) ? date('M j, Y @ g:ia', strtotime($job->modified)) :"" ?></p>
	
		</div>
	</div>
<?php endif;?>
