<div class="row">
	<div class="col-md-7 dashboard">	
		<h3><i class='fa fa-clock-o'></i> My Timesheets</h3>
			<!-- <form class="form-horizontal" method="post" enctype="multipart/form-data">
				<div class="form-group">
					<div class="col-md-6">
						<?php $selected_limit = $_COOKIE['timesheets_to_show']; ?>
						<?= form_dropdown('limit', $timesheetLimit , $selected_limit, 'class="form-control" id="limit" name="limit"') ?>
					</div>
					<div class="col-md-6">
						<input class="btn btn-default" type="submit" value="Submit" />		
					</div>	
				</div>
			</form>-->
		<table class="table table-striped tablesorter">
			<thead class="thead">
				<th>Week Ending</th>
				<th class="center">Total Time</th>
				<td class="center">Status</td>
			</thead>
			<tbody>

			<?php foreach ($timesheets as $timesheet) { ?>
				<tr class="">
					<td><a href="<?= base_url() ?>timesheets/update/<?= $timesheet->timesheet_id ?>"><?= $timesheet->week_ending ?></a></td>
					<td class="center"><?= isset($timesheet->total)? $timesheet->total : '0.00'; ?></td>				
					<td class="center"><?= $timesheet->status ?></td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
		<div class="pagination">
			<?= $links ?> 
		</div>
	</div>
	<div class="col-md-4 dashboard pull-right">
		<h3><i class='fa fa-calendar'></i> My Time Off</h3>
		<table class="table table-striped tablesorter">
			<thead class="thead">
				<th>Type</th>
				<th>Hours</th>
				<th>Days</th>
			</thead>
			<tbody>
				<?php if(!empty($nonBillable)):?>
					<?php foreach ($nonBillable as $each) { ?>
						<tr class="">
							<td><?= $each->task_name ?></td>
							<td><?= $each->total ?></td>
							<td><?= $each->days ?></td>
						</tr>
					<?php } ?>
				<?php endif;?>
			</tbody>
		</table>
	</div>
</div>