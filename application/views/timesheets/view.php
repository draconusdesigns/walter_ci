<script type="text/javascript" src="<?= asset_url() ?>/js/timesheet.js"></script>

<div class="row">
	<div class="col-md-12">
		<table class="table table-striped tablesorter">
			<thead class="thead">
				<th>Job</th>
				<th>Sun <?= date('m/d', strtotime(' -6 days', strtotime($timesheet->week_ending))); ?></th>
				<th>Mon <?= date('m/d', strtotime(' -5 days', strtotime($timesheet->week_ending))); ?></th>
				<th>Tues <?= date('m/d', strtotime(' -4 days', strtotime($timesheet->week_ending))); ?></th>
				<th>Wed <?= date('m/d', strtotime(' -3 days', strtotime($timesheet->week_ending))); ?></th>
				<th>Thurs <?= date('m/d', strtotime(' -2 days', strtotime($timesheet->week_ending))); ?></th>
				<th>Fri <?= date('m/d', strtotime(' -1 day', strtotime($timesheet->week_ending))); ?></th>
				<th>Sat <?= date('m/d', strtotime($timesheet->week_ending)); ?></th>
				<td class="center">Total</td>
			</thead>
			<tbody>
			<?php if($tasks): ?>
				<?php foreach ($tasks as $task): ?>
					<tr class="">
						<td class="title">
							<?php if(!empty($task->task_type_name)): ?>
								<small>&nbsp;</small>
								<h4><?= $task->task_type_name?></h4>
							<?php else: ?>
								<small>Job # <?= $task->job_id ?></small>
								<h4><a href="<?= base_url() ?>jobs/view/<?= $task->job_id ?>"><?= $task->job_name?></a></h4>
							<?php endif; ?>
							
							<input type="hidden" name="task_id[]" value="<?= set_value("task_id[]", $task->task_id) ?>" />
						</td>
						<td class="center">
							<input data-rel="sun" data-id="<?= $task->task_id ?>" type="text" name="sun[]" class="form-control parsable sun task-<?= $task->task_id ?>" value="<?= set_value("sun[]", $task->sun) ?>"/>
						</td>                         
						<td class="center">           
							<input data-rel="mon" data-id="<?= $task->task_id ?>" type="text" name="mon[]" class="form-control parsable mon task-<?= $task->task_id ?>" value="<?= set_value("mon[]", $task->mon) ?>"/>
						</td>					      
						<td class="center">           
							<input data-rel="tue" data-id="<?= $task->task_id ?>" type="text" name="tue[]" class="form-control parsable tue task-<?= $task->task_id ?>" value="<?= set_value("tue[]", $task->tue) ?>"/>
						</td>					      
						<td class="center">           
							<input data-rel="wed" data-id="<?= $task->task_id ?>" type="text" name="wed[]" class="form-control parsable wed task-<?= $task->task_id ?>" value="<?= set_value("wed[]", $task->wed) ?>"/>
						</td>					      
						<td class="center">           
							<input data-rel="thu" data-id="<?= $task->task_id ?>" type="text" name="thu[]" class="form-control parsable thu task-<?= $task->task_id ?>" value="<?= set_value("thu[]", $task->thu) ?>"/>
						</td>					      
						<td class="center">           
							<input data-rel="fri" data-id="<?= $task->task_id ?>" type="text" name="fri[]" class="form-control parsable fri task-<?= $task->task_id ?>" value="<?= set_value("fri[]", $task->fri) ?>"/>
						</td>					
						<td class="center">
							<input data-rel="sat" data-id="<?= $task->task_id ?>" type="text" name="sat[]" class="form-control parsable sat task-<?= $task->task_id ?>" value="<?= set_value("sat[]", $task->sat) ?>"/>
						</td>					
						<td class="total center" id="<?= $task->task_id ?>" class="center"><h4><?= $task->total?></h4></td>
					</tr>
				<?php endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="10">
						<div class="alert alert-dismissable alert-danger">
							No Jobs are Currently Attached to this timesheet. Use the form above to add one now.
						</div>
					</td>
				</tr>
			<?php endif; ?>
			</tbody>
			<tfoot class="thead">
				<th>Totals</th>
				<th id="sun" class="center total"></th>
				<th id="mon" class="center total"></th> 
				<th id="tue" class="center total"></th> 
				<th id="wed" class="center total"></th>
				<th id="thu" class="center total"></th> 
				<th id="fri" class="center total"></th> 
				<th id="sat" class="center total"></th> 
				<th class="center" id="total" data-rel="<?= $timesheet->timesheet_id ?>" class="center total"></th>
			</tfoot>
		</table>	
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function ( ) {        
		$('input[type=text], select, .add-button, .remove_task').prop("disabled", true);
	});
</script>