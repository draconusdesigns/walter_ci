<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config = array(
    'job_validation' => array(
		array(
			'field' => 'job_name',
			'label' => 'Job Name',
			'rules' => 'trim|xss_clean|required'
		),
		array(
			'field' => 'status_id',
			'label' => 'Job Status',
			'rules' => 'trim|xss_clean'
		),
		array(
			'field' => 'user_id',
			'label' => 'Job Owner',
			'rules' => 'trim|xss_clean|required'
		),
		array(
			'field' => 'job_type_id',
			'label' => 'Job Type',
			'rules' => 'trim|xss_clean|required'
		),
		array(
			'field' => 'marketing_region_id',
			'label' => 'Market Region',
			'rules' => 'trim|xss_clean|required'
		),
		array(
			'field' => 'business_unit_id',
			'label' => 'Business Unit',
			'rules' => 'trim|xss_clean|required'
		),
		array(
			'field' => 'category_id',
			'label' => 'Category',
			'rules' => 'trim|xss_clean|required'
		),
		array(
			'field' => 'vehicle_id',
			'label' => 'Vehicle',
			'rules' => 'trim|xss_clean|required'
		),
		array(
			'field' => 'tier_id',
			'label' => 'Tier',
			'rules' => 'trim|xss_clean|required'
		),
		array(
			'field' => 'mp_description',
			'label' => 'Portal Description',
			'rules' => 'trim|xss_clean'
		),
		array(
			'field' => 'mp_search',
			'label' => 'Portal Search Keywords',
			'rules' => 'trim|xss_clean'
		),
		array(
			'field' => 'hit_date',
			'label' => 'Hit Date',
			'rules' => 'trim|xss_clean|required'
		),
		array(
			'field' => 'notes',
			'label' => 'Job Notes',
			'rules' => 'trim|xss_clean'
		)
    ),
	'timesheet_validation' => array(
		array(
			'field' => 'sun[]',
			'label' => 'Sunday',
			'rules' => 'trim|numeric|xss_clean'
		),
		array(
			'field' => 'mon[]',
			'label' => 'Monday',
			'rules' => 'trim|numeric|xss_clean'
		),
		array(
			'field' => 'tue[]',
			'label' => 'Tuesday',
			'rules' => 'trim|numeric|xss_clean'
		),
		array(
			'field' => 'wed[]',
			'label' => 'Wednesday',
			'rules' => 'trim|numeric|xss_clean'
		),
		array(
			'field' => 'thu[]',
			'label' => 'Thursday',
			'rules' => 'trim|numeric|xss_clean'
		),
		array(
			'field' => 'fri[]',
			'label' => 'Friday',
			'rules' => 'trim|numeric|xss_clean'
		),
		array(
			'field' => 'sat[]',
			'label' => 'Saturday',
			'rules' => 'trim|numeric|xss_clean'
		)
	),
	'business_validation' => array(
		array(
			'field' => 'category_id',
			'label' => 'Category',
			'rules' => 'trim|xss_clean'
		),
		array(
			'field' => 'vehicle_id',
			'label' => 'Vehicle',
			'rules' => 'trim|xss_clean'
		)
	),
	'ratecard_validation' => array(
		array(
			'field' => 'marketing_region_id',
			'label' => 'Market Region',
			'rules' => 'trim|xss_clean|required'
		),
		array(
			'field' => 'business_unit_id',
			'label' => 'Business Unit',
			'rules' => 'trim|xss_clean|required'
		),
		
		array(
			'field' => 'vehicle_id',
			'label' => 'Vehicle',
			'rules' => 'trim|xss_clean|required'
		),
		array(
			'field' => 'tier_id',
			'label' => 'Tier',
			'rules' => 'trim|xss_clean|required'
		),
		array(
			'field' => 'rate',
			'label' => 'Average Hours',
			'rules' => 'trim|xss_clean|required'
		)
    ),
	'task_validation' => array(
		array(
			'field' => 'job_id',
			'label' => 'Job ID',
			'rules' => 'trim|xss_clean'
		),
		array(
			'field' => 'effort_code',
			'label' => 'Effort Code',
			'rules' => 'trim|xss_clean'
		),
		array(
			'field' => 'task_type_id',
			'label' => 'Task Type',
			'rules' => 'trim|xss_clean'
		)
	)
);