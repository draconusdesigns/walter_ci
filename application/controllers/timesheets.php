<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Timesheets extends MY_Controller {
	
	function __construct(){
		parent::__construct();
		
	}
	function index() {
		$user_id = $this->session->userdata('user_id');
				
		//Creates a timesheet for this week for user if one doesn't already exist.
		if($this->timesheet_model->createTimesheet($user_id)):
			redirect(base_url().'timesheets/index/', 'refresh');
		endif;
		
		$data['titleTag'] = "Welcome";
		$data['pageHeading'] = "<i class='fa fa-dashboard'></i> Walter";
		$data['pageSubHeading'] = "";
		
		$config['base_url'] = base_url().'timesheets/index/';
		$config['total_rows'] = $this->timesheet_model->countTimesheets($user_id);
		$config['per_page'] = 10;
		$this->pagination->initialize($config);
		
		$data['timesheets'] = $this->timesheet_model->getPaginatedTimesheets($user_id, $config['per_page'], $this->uri->segment(3));
 
		$data['links'] = $this->pagination->create_links();

		$data['nonBillable'] = $this->timesheet_model->getTimesheetsNonBillable($user_id);

		$this->load->view('templates/header', $data);
		$this->load->view('timesheets/index', $data);
		$this->load->view('templates/footer', $data);
	}
	
	function view($timesheet_id) {
		$data['timesheet'] = $this->timesheet_model->getTimesheet($timesheet_id);
		$data['tasks'] = $this->timesheet_model->getTasksForTimesheet($timesheet_id);		
		
		$week_ending = date( 'm/d/Y', strToTime($data['timesheet']->week_ending));
		
		$data['titleTag'] = "Timesheet View";
		$data['pageHeading'] = "View Timesheet";
		$data['pageSubHeading'] = "for Week Ending: {$week_ending}";
		
		$this->load->view('templates/header', $data);
		$this->load->view('timesheets/view', $data);
		$this->load->view('templates/footer', $data);
	}
	
	function update($timesheet_id) {
		$user_id = $this->session->userdata('user_id');
		
		$data['timesheet'] = $this->timesheet_model->getTimesheet($timesheet_id);
		
		if($data['timesheet']->status_id == 1):
			$data['timesheet']->status = 'Active';
		else:
			$data['timesheet']->status = 'Complete';
		endif;
	
		$data['tasks'] = $this->timesheet_model->getTasksForTimesheet($timesheet_id);
		
		//Redirect Users From Timesheet Update to View Only if the timesheet does not belong to them
		if($data['timesheet']->user_id != $user_id):
			redirect('/timesheets/view/'.$timesheet_id, 'refresh');
		endif;
		
		$data['taskTypes'] = $this->timesheet_model->getTaskTypes();

		$week_ending = date( 'm/d/Y', strToTime($data['timesheet']->week_ending));
		
		$data['titleTag'] = "Timesheet Editor";
		$data['pageHeading'] = "<i class='fa fa-clock-o'></i> Update Timesheet";
		$data['pageSubHeading'] = "for Week Ending: {$week_ending}";
		
		if ($this->form_validation->run('task_validation') == FALSE){				
			$this->load->view('templates/header', $data);
			$this->load->view('timesheets/update', $data);
			$this->load->view('templates/footer', $data);
		} else {
			//insert task if it is not already in this timesheet
			$this->timesheet_model->addTask($data);
			redirect('/timesheets/update/'.$timesheet_id, 'location');
		}
	}
	
	function update_field($task_id, $day, $value) {
		$this->timesheet_model->updateTimesheet($task_id, $day, $value);
	}
	
	function update_status($timesheet_id, $status_id) {
		$user_id = $this->session->userdata('user_id');
		
		if($status_id == '2'):
			$this->timesheet_model->removeEmptyTasks($user_id, $timesheet_id);
		endif;
		
		$this->timesheet_model->updateTimesheetStatus($timesheet_id, $status_id);
	}
	
	function remove_task($timesheet_id, $task_id){
		$user_id = $this->session->userdata('user_id');
		$this->timesheet_model->removeTask($user_id, $timesheet_id, $task_id);
	}
	
	function copy_tasks($timesheet_id){
		$user_id = $this->session->userdata('user_id');
		$this->timesheet_model->copyTasks($user_id, $timesheet_id);
	}
}