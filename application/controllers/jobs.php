<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jobs extends MY_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->helper('date');
		
		$this->load->model('user_model');
						
		//$this->output->enable_profiler(TRUE);
		$this->job_search_terms = $this->session->userdata('job_search_terms');
	}
	
	function index() {
		//if there is an issue, uncomment these lines to see what is passed in
		// echo '<pre>';
		// print_r($this->job_search_terms);
		// exit;
		$data['titleTag'] = "Manage Jobs";
		$data['pageHeading'] = "<i class='fa fa-briefcase'></i> Manage Jobs";
		$data['statuses'] = $this->job_model->getStatusDropdown();
		$data['job_search_terms'] = $this->job_search_terms;
		
		$config['base_url'] = base_url().'/jobs/index/';
		$config['total_rows'] = $this->job_model->countJobs($this->job_search_terms);
		$config['per_page'] = 25;
		$this->pagination->initialize($config);
		
		$data['allJobs'] = $this->job_model->getPaginatedJobs($config['per_page'], $this->uri->segment(3), $this->job_search_terms);
		 		
		$data['links'] = $this->pagination->create_links();
		$this->load->view('templates/header', $data);
		$this->load->view('jobs/index', $data);
		$this->load->view('templates/footer', $data);
	}
	
	function search(){
		//Get keys and values and pass them into the createSearch Array function
		$terms = $this->input->post('terms');
		$job_terms = $this->job_search_terms;
		$job_terms[] = str_replace('"', '', $terms);
		$this->session->set_userdata('job_search_terms', $job_terms);
		$this->json(array('response'=>'success'));
	}
	
	function remove_term($term=null) {
		$term = $this->input->post('term') ? $this->input->post('term') : $term;
		$job_terms = $this->job_search_terms;
		$i = 0;
		foreach ( $job_terms as $job_term ) {
			if ( $job_term == $term ) {
				unset($job_terms[$i]);
			}
			$i++;
		}
		$this->session->set_userdata('job_search_terms', $job_terms);
		$this->json(array('response'=>'success'));
	}
	
	function search_reset() { 
		$this->session->set_userdata('job_search_terms', NULL);
		$this->session->unset_userdata('job_search_terms');
		redirect('jobs/index', 'refresh');
	}
	
	function add() {				
		$data['titleTag'] = 'Job Setup';
		$data['pageHeading'] = "<i class='fa fa-plus-circle'></i> Add Job";
		$data['pageSubHeading'] = "* Required fields marked with an asterisk";
				
		$data['statuses'] = $this->job_model->getStatusDropdown();
		$data['users'] = $this->user_model->getOwnerDropdown();
		$data['jobTypes'] = $this->job_model->getTypeDropdown();
		$data['marketingRegions'] = $this->job_model->getRegionsDropdown();
		$data['businessUnits'] = $this->business_model->getBuDropdown();
		$data['categories'] = $this->business_model->getCategoryDropdown();
		$data['vehicles'] = $this->business_model->getVehicleDropdown();
		
		$data['tiers'] = $this->job_model->getTierDropdown();

 		if ($this->form_validation->run('job_validation') == FALSE){
			$this->load->view('templates/header', $data);
			$this->load->view('jobs/update', $data);
			$this->load->view('templates/footer', $data);
			
		} else {
			//insert job
			$this->job_model->addJob($data);

			redirect('/jobs', 'refresh');
		}
	}
	
	function edit($job_id) {
			
		$data['job'] = $this->job_model->getJob($job_id);
		$data['job_hours'] = $this->job_model->getHoursPerJob($job_id);
			
		$data['titleTag'] = 'Job Setup';
		$data['pageHeading'] = "<i class='fa fa-edit'></i> Edit Job";
		$data['pageSubHeading'] = "* Required fields marked with an asterisk";

		$data['statuses'] = $this->job_model->getStatusDropdown();
		$data['users'] = $this->user_model->getOwnerDropdown();
		$data['jobTypes'] = $this->job_model->getTypeDropdown();
		$data['marketingRegions'] = $this->job_model->getRegionsDropdown();
		$data['businessUnits'] = $this->business_model->getBuDropdown();
		$data['categories'] = $this->business_model->getCategoryDropdown($data['job']->business_unit_id);
		$data['vehicles'] = $this->business_model->getVehicleDropdown($data['job']->business_unit_id);
		
		$data['tiers'] = $this->job_model->getTierDropdown();

		if ($this->form_validation->run('job_validation') == FALSE){
			$this->load->view('templates/header', $data);
			$this->load->view('jobs/update', $data);
			$this->load->view('templates/footer', $data);
		} else {
			//update Job
			$this->job_model->updateJob($data);
			
			redirect('jobs/index', 'location');
		}
		
	}
	
	function duplicate(){
		$job_id = $this->job_model->duplicateJob($_POST['job_id']);
		$this->session->set_flashdata('success','Job Duplicated Successfully <br> NOTE: Set a Hit Date');
		redirect('jobs/edit/'.$job_id, 'location');
	}
	
	function get_categories(){
		$business_unit_id = $this->input->post('business_unit_id');

	 	$categories = $this->business_model->getCategoryDropdown($business_unit_id);
	
 		return $this->json($categories);
	}
	
	function get_vehicles(){
		$business_unit_id = $this->input->post('business_unit_id');

		$vehicles = $this->business_model->getVehicleDropdown($business_unit_id);
		
 		return $this->json($vehicles);
	}
		
	function view($job_id) {	
		$data['titleTag'] = 'Job Setup';
		$data['pageHeading'] = "<i class='fa fa-eye'></i> View Job Profile";
		$data['job'] = $this->job_model->getJobView($job_id);
		$data['job_hours'] = $this->job_model->getHoursPerJob($job_id);

		$this->load->view('templates/header', $data);
		$this->load->view('jobs/view', $data);
		$this->load->view('templates/footer', $data);
	}
	
	function update_status($status_id, $job_id){
		$this->job_model->updateStatus($status_id, $job_id);
	}
	
	function addToTimesheet($job_id){		
		$user_id = $this->session->userdata('user_id');
		$data['timesheet'] = $this->timesheet_model->getCurrentTimesheet($user_id);
		
		if(!empty($data['timesheet']->timesheet_id)):
			$this->timesheet_model->addTasktoCurrentTimesheet($job_id, $data['timesheet']->timesheet_id);
		else:
			$this->session->set_flashdata('error','There is no current timesheet for this user');
		endif;
		
		redirect('jobs/index', 'refresh');
	}
}