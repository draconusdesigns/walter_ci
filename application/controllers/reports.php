<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends MY_Controller {
	
	function __construct(){
		parent::__construct();

		$this->load->model('ratecard_model');
				
		//$this->output->enable_profiler(TRUE);
	}
	function index() {
		$data['titleTag'] = "Reports";
		$data['pageHeading'] = "<i class='fa fa-bar-chart-o'></i> Reports";
		
		$this->load->view('templates/header', $data);
		$this->load->view('reports/index', $data);
		$this->load->view('templates/footer', $data);
	}
	
	function ratecards() {
		$data['titleTag'] = "Ratecards";
		$data['pageHeading'] = "<i class='fa fa-bullseye'></i> Ratecards";
		
		$config['base_url'] = base_url().'/reports/ratecards/';
		$config['total_rows'] = $this->ratecard_model->countRatecards();
		$config['per_page'] = 100;
		$this->pagination->initialize($config);
		
		$data['allRatecards'] = $this->ratecard_model->getPaginatedRatecards($config['per_page'], $this->uri->segment(3));		
		$data['links'] = $this->pagination->create_links();
		
		$this->load->view('templates/header', $data);
		$this->load->view('reports/ratecards/index', $data);
		$this->load->view('templates/footer', $data);
	}
	
	function add_ratecard() {				
		$data['titleTag'] = 'Ratecard Setup';
		$data['pageHeading'] = "<i class='fa fa-plus-circle'></i> Add Ratecard";
		$data['pageSubHeading'] = "* Required fields marked with an asterisk";
				
		$data['marketingRegions'] = $this->job_model->getRegionsDropdown();
		$data['businessUnits'] = $this->business_model->getBuDropdown();
		$data['vehicles'] = $this->business_model->getVehicleDropdown();
		$data['tiers'] = $this->job_model->getTierDropdown();

 		if ($this->form_validation->run('ratecard_validation') == FALSE){
			$this->load->view('templates/header', $data);
			$this->load->view('reports/ratecards/update', $data);
			$this->load->view('templates/footer', $data);
			
		} else {
			//insert Ratecard
			$this->ratecard_model->addRatecard($data);

			redirect('/reports/ratecards', 'refresh');
		}
	}
	
	function edit_ratecard($rate_card_id) {	
		$data['ratecard'] = $this->ratecard_model->getRatecard($rate_card_id);
			
		$data['titleTag'] = 'Ratecard Setup';
		$data['pageHeading'] = "<i class='fa fa-edit'></i> Edit Ratecard";
		$data['pageSubHeading'] = "* Required fields marked with an asterisk";

		$data['marketingRegions'] = $this->job_model->getRegionsDropdown();
		$data['businessUnits'] = $this->business_model->getBuDropdown();
		$data['vehicles'] = $this->business_model->getVehicleDropdown($data['ratecard']->business_unit_id);
		
		$data['tiers'] = $this->job_model->getTierDropdown();

		if ($this->form_validation->run('ratecard_validation') == FALSE){
			$this->load->view('templates/header', $data);
			$this->load->view('reports/ratecards/update', $data);
			$this->load->view('templates/footer', $data);
		} else {
			//update Ratecard
			$this->ratecard_model->updateRatecard($data);
			
			redirect('/reports/ratecards', 'refresh');
		}
	}
	
	function view_ratecard($rate_card_id) {		
		$data['titleTag'] = 'View Ratecard';
		$data['pageHeading'] = "<i class='fa fa-bullseye'></i>View Ratecard";
		$data['ratecard'] = $this->ratecard_model->getRatecardView($rate_card_id);
		
		$this->load->view('templates/header', $data);
		$this->load->view('reports/ratecards/view', $data);
		$this->load->view('templates/footer', $data);
	}
}