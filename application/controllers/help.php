<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Help extends MY_Controller {
	
	function __construct(){
		parent::__construct();	
		//$this->output->enable_profiler(TRUE);
	}
	function index() {
		$data['titleTag'] = "Help";
		$data['pageHeading'] = "Walter Help";
		
		$this->load->view('templates/header', $data);
		$this->load->view('help/index', $data);
		$this->load->view('templates/footer', $data);
	}
}