<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Business extends MY_Controller {
	
	function __construct(){
		parent::__construct();	
				
		//$this->output->enable_profiler(TRUE);
	}
	function index() {
		$data['titleTag'] = "Business Units";
		$data['pageHeading'] = "<i class='fa fa-cubes'></i> Business Units";
				
		$config['base_url'] = base_url().'/business/index/';
		$config['total_rows'] = $this->business_model->countUnits();
		$config['per_page'] = 50;
		$this->pagination->initialize($config);

		$data['units'] = $this->business_model->getPaginatedUnits($config['per_page'], $this->uri->segment(3));		
		$data['links'] = $this->pagination->create_links();
		
		$this->load->view('templates/header', $data);
		$this->load->view('business/index', $data);		
		$this->load->view('templates/footer', $data);
	}
	function update($business_unit_id) {
		$data['unit'] = $this->business_model->getBusinessUnit($business_unit_id);		

		$data['titleTag'] = "Update Business Unit";
		$data['pageHeading'] = "<i class='fa fa-cubes'></i> " . $data['unit']->business_unit . " Business Unit";
		
		$data['allCategories'] = $this->business_model->getCategoryUnitDropdown();
		$data['categories'] = $this->business_model->getCategories($business_unit_id);
		
		$data['allVehicles'] = $this->business_model->getVehicleUnitDropdown();
		$data['vehicles'] = $this->business_model->getVehicles($business_unit_id);
		
		if ($this->form_validation->run('business_validation') == FALSE){
			$this->load->view('templates/header', $data);
			$this->load->view('business/update', $data);		
			$this->load->view('templates/footer', $data);
		} else {
			$this->business_model->updateBusinessUnit($data);

			redirect('business/update/'.$business_unit_id, 'refresh');
		}
	}
	function remove_category($catalog_id, $business_unit_id) {	
		$this->business_model->removeCategory($catalog_id);
		redirect('business/update/'.$business_unit_id, 'refresh');
	}
	
	function remove_vehicle($catalog_id, $business_unit_id) {
		$this->business_model->removeVehicle($catalog_id);
		redirect('business/update/'.$business_unit_id, 'refresh');
	}
}