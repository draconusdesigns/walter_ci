<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tools extends MY_Controller {
	
	function __construct(){
		parent::__construct();
		
        $this->is_of_role(2) || redirect(base_url(), 'refresh');
		
		$this->load->model('user_model');
	}
	
	function index() {
		/* Gets the user_id from the currently logged in user */
		$user_id = $this->session->userdata('user_id');
		
		$data['titleTag'] = "Manager Tools";
		$data['pageHeading'] = "<i class='fa fa-wrench'></i> Manager Tools";
		$data['pageSubHeading'] = "";
		
		/* Gets all subordinates for the user_id passed in */
		$data['subs'] = $this->user_model->getSubordinants($user_id);
		
		$this->load->view('templates/header', $data);
		$this->load->view('tools/index', $data);
		$this->load->view('templates/footer', $data);
	}
	
	function timesheets($user_id = null) {
		$data['user'] = $this->user_model->getUser($user_id);
		$data['titleTag'] = "Timesheets";
		$data['pageHeading'] = "<i class='fa fa-clock-o'></i> Timesheets for {$data['user']->name}";
		$data['pageSubHeading'] = "";
		
		$config['base_url'] = base_url()."tools/timesheets/{$user_id}/";
		$config['total_rows'] = $this->timesheet_model->countTimesheets($user_id);
		$config['per_page'] = 10;
		$config['uri_segment'] = 4;
		$this->pagination->initialize($config);
		
		$data['timesheets'] = $this->timesheet_model->getPaginatedTimesheets($user_id, $config['per_page'], $this->uri->segment(4));
 
		$data['links'] = $this->pagination->create_links();
		
		$data['nonBillable'] = $this->timesheet_model->getTimesheetsNonBillable($user_id);

		$this->load->view('templates/header', $data);
		$this->load->view('tools/timesheets', $data);
		$this->load->view('templates/footer', $data);
	}
	
	function view($timesheet_id) {
		$data['timesheet'] = $this->timesheet_model->getTimesheet($timesheet_id);
		$data['tasks'] = $this->timesheet_model->getTasksForTimesheet($timesheet_id);
		$data['total'] = $this->timesheet_model->getTaskTotals($timesheet_id);
		
		$week_ending = date( 'm/d/Y', strToTime($data['timesheet']->week_ending));
		
		$data['titleTag'] = "Timesheet View";
		$data['pageHeading'] = "View Timesheet";
		$data['pageSubHeading'] = "for Week Ending: {$week_ending}";
		
		$this->load->view('templates/header', $data);
		$this->load->view('timesheets/view', $data);
		$this->load->view('templates/footer', $data);
	}
	
	function update($timesheet_id) {
		$data['timesheet'] = $this->timesheet_model->getTimesheet($timesheet_id);
		$data['tasks'] = $this->timesheet_model->getTasksForTimesheet($timesheet_id);
		$data['total'] = $this->timesheet_model->getTaskTotals($timesheet_id);

		$week_ending = date( 'm/d/Y', strToTime($data['timesheet']->week_ending));
		
		$data['titleTag'] = "Timesheet Editor";
		$data['pageHeading'] = "<i class='fa fa-clock-o'></i> Update Timesheet";
		$data['pageSubHeading'] = "for Week Ending: {$week_ending}";
		
		if ($this->form_validation->run('task_validation') == FALSE){				
			$this->load->view('templates/header', $data);
			$this->load->view('timesheets/update', $data);
			$this->load->view('templates/footer', $data);
		} else {
			//insert task if it is not already in this timesheet
			$this->timesheet_model->addTask($data);
			redirect('/timesheets/update/'.$timesheet_id, 'refresh');
		}
	}
	
	function update_field($task_id, $day, $value) {
		$this->timesheet_model->updateTimesheet($task_id, $day, $value);
	}
}