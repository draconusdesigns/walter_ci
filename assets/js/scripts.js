$(document).ready(function() {
	$("table.tablesorter").tablesorter();
	
	$('#datepicker').datepicker({
	    format: 'yyyy-mm-dd',
		todayHighlight: true,
		autoclose: true
	})
	
	
	$('#datepicker_hit_date').datepicker({
	    format: 'yyyy-mm-dd',
		todayHighlight: true,
		autoclose: true
	})
	
	$('#datepicker_release_date').datepicker({
	    format: 'yyyy-mm-dd',
		todayHighlight: true,
		autoclose: true
	})

	
	$('#weekend').datepicker({
		format: "yyyy-mm-dd",
		daysOfWeekDisabled: "0,1,2,3,4,5",
		autoclose: true,
		todayHighlight: true
	})
	
	$(document).ready(function() {    
		$(".flag").click(function () {
	        $(".flag").fadeOut("slow");
	    });

	    //fade out in 5 seconds if not closed
	    $(".flag").delay(5000).fadeOut("slow");
	})
});