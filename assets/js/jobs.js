$(document).ready(function () {	
	var business_unit_id = $('#business_unit_id').val();
	if($('#business_unit_id option:selected').val() != 9){
		$('#label_effort_code').hide();
		$('#effort_code').hide();
	}
	
	$('#business_unit_id').change(function () { 
        var business_unit_id = $('#business_unit_id').val();

		get_categories(business_unit_id);
		get_vehicles(business_unit_id);
		
		if($('#business_unit_id option:selected').val() == 9){
			$('#label_effort_code').show();
			$('#effort_code').show();
		}else{
			$('#label_effort_code').hide();
			$('#effort_code').hide();
		}
	});

	function get_categories(business_unit_id) {    
		$("#category_id > option").remove(); 
		$.post(
			base_url + "jobs/get_categories",
			{
				business_unit_id:business_unit_id
			},
			function(data) {
				data = sort_json(data);
				$.each(data, function(category_id, category) {
					var opt = $('<option />');
					opt.val(category_id.num);
					opt.text(category.word);
					$('#category_id').append(opt);
				})
			}, 'json'
		);
	}
	
	function get_vehicles(business_unit_id) {    
		$("#vehicle_id > option").remove(); 
		    
		 $.post(
			base_url + "jobs/get_vehicles",
			{
				business_unit_id:business_unit_id
			},
			function(data) {
				data = sort_json(data);
				$.each(data, function(vehicle_id, vehicle) {
					var opt = $('<option />');
					opt.val(vehicle_id.num);
					opt.text(vehicle.word);
					$('#vehicle_id').append(opt);
				})
			}, 'json'
		);
	}
	
	function sort_json(obj) {
		var dataArray = [];
		for (num in obj) {
		    var word = obj[num];
		    dataArray.push({num: parseInt(num), word: word});
		}

		dataArray.sort(function(a, b){
		    if (a.word < b.word) return -1;
		    if (b.word < a.word) return 1;
		    return 0;
		});
		
		return dataArray;
	}
	
});