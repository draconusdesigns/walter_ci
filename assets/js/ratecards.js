$(document).ready(function () {	
	var business_unit_id = $('#business_unit_id').val();	

    $('#business_unit_id').change(function () { 
        var business_unit_id = $('#business_unit_id').val();

		get_vehicles(business_unit_id);
	});
	
	function get_vehicles(business_unit_id) {    
		$("#vehicle_id > option").remove(); 
		    
		 $.post(
			base_url + "jobs/get_vehicles",
			{
				business_unit_id:business_unit_id
			},
			function(data) {
				data = sort_json(data);
				
				$.each(data, function(category_id, category) {
					var opt = $('<option />');
					opt.val(category_id.num);
					opt.text(category.word);
					$('#vehicle_id').append(opt);
				})
			}, 'json'
		);
	}
	
	function sort_json(obj) {
		
		/*
		* returns a JSON array accessible by key => value using key.name => val.word
		*/
		
		var dataArray = [];
		for (num in obj) {
		    var word = obj[num];
		    dataArray.push({num: parseInt(num), word: word});
		}

		dataArray.sort(function(a, b){
		    if (a.word < b.word) return -1;
		    if (b.word < a.word) return 1;
		    return 0;
		});
		
		return dataArray;
	}
});