$(document).ready(function ( ) {        
		if ($("#status_id").val() == "1") {
			 $('input[type=text], select, .add-button, .remove_task').prop("disabled", true);
		}
		
		calcDays('sun');
		calcDays('mon');
		calcDays('tue');
		calcDays('wed');
		calcDays('thu');
		calcDays('fri');
		calcDays('sat');
		
		calcTasks();
		
		calcGrandTotal();
		
	// apply the two-digits behaviour to elements with 'two-digits' as their class
	$( function() {
	    $('.parsable').keyup(function(){
	        if($(this).val().indexOf('.')!=-1){         
	            if($(this).val().split(".")[1].length > 2){                
	                if( isNaN( parseFloat( this.value ) ) ) return;
	                this.value = parseFloat(this.value).toFixed(2);
	            }  
	         }            
	         return this; //for chaining
	    });
	});

	$('input').change(function ( ) {
		var day = $(this).data('rel');
		var id = $(this).data('id');
		var value = parseFloat(this.value);
		update(id, day, value);
		
		calcDays(day);
		calcTasks(id);
		
		calcGrandTotal();
	});
	
	$("#update-status").click(function(e){
	    e.preventDefault();
		
		var timesheet_id = $("#timesheet_id").val();
		var status_id = $("#status_id").val();
		
		updateStatus(timesheet_id, status_id);
	});
	
	$("#copy-tasks").click(function(e){
	    e.preventDefault();
		var timesheet_id = $(this).closest('form').find('input[name="timesheet_id"]').val();
		var user_id = $(this).closest('form').find('input[name="user_id"]').val();
		copy_tasks(timesheet_id);
	});
	
	$(".remove_task").click(function(e){
	    e.preventDefault();
		var timesheet_id = $(this).closest('form').find('input[name="timesheet_id"]').val();
		var task_id = $(this).closest('form').find('input[name="task_id"]').val();
		removeTask(timesheet_id, task_id);
	});
});
function update(id, day, value) {    
	 $.ajax({
        type: "POST",
        url: base_url + "timesheets/update_field/" + id + "/" + day + "/" + value,
	 });
}

function updateStatus(timesheet_id, status_id) {    
	 $.ajax({
        type: "POST",
        url: base_url + "timesheets/update_status/" + timesheet_id + "/" + status_id,
		success: window.location.href = base_url + 'timesheets/update/' + timesheet_id,
	 });
}

function copy_tasks(timesheet_id) {    
	 $.ajax({
        type: "POST",
        url: base_url + "timesheets/copy_tasks/" + timesheet_id,
		success: window.location.href = base_url + 'timesheets/update/' + timesheet_id,
	 });
}

function removeTask(timesheet_id, task_id) {    
	 $.ajax({
        type: "POST",
        url: base_url + "timesheets/remove_task/" + timesheet_id + "/" + task_id,
		success: window.location.href = base_url + 'timesheets/update/' + timesheet_id,
	 });
}

function calcDays(day) {
	var sum = 0;
	$('.' + day).each(function ( ) {
        if(!isNaN(this.value) && this.value.length!=0) {
            sum += parseFloat(this.value);
		}
	});
	$('#' + day).html(sum.toFixed(2));
}

function calcTasks(id) {
	var sum = 0;
	$('.task-' + id).each(function ( ) {
        if(!isNaN(this.value) && this.value.length!=0) {
            sum += parseFloat(this.value);
		}
	});
	$('#' + id).html('<h4>' + sum.toFixed(2) + '</h4>');
}

function calcGrandTotal() {
	var sum = 0;
	
	$('.parsable').each(function ( ) {
        if(!isNaN(this.value) && this.value.length!=0) {
            sum += parseFloat(this.value);
		}
	});
	$('#total').html(sum.toFixed(2));
}

$('#limit').change(function() {

	document.location='index.php?limit=\'+this.value';

});